/******************************************************************************/
/**
  @file          das_d_outputs.h
  @brief         Description of this file (usually copied from the module).
  */                 
/*  Revision: */
/*  Jun 29, 2020        Rev/Sprint  Initials  Remarks */ 
/******************************************************************************/
#ifndef HDR_DAS_D_OUTPUTS_H_
#define HDR_DAS_D_OUTPUTS_H_

/* Includes *******************************************************************/
#include "hdr/global.h"

#include "driverlib/timer.h"

#include "inc/hw_memmap.h"


/** @defgroup _das_d_outputs das_d_outputs
  * @brief Describe module
  * @{
  */  

/** @defgroup _das_d_outputs_Header Header
  * @{
  */  

/* Public Macros **************************************************************/
/** @defgroup _das_d_outputs_Public_Macros Public Macros
  * @{
  */

//Digital Outputs for Port N
#define DOUTN_PERIPH                         SYSCTL_PERIPH_GPION
#define DOUTN_PORT                           GPIO_PORTN_BASE

//Digital Outputs for Port L
#define DOUTL_PERIPH                         SYSCTL_PERIPH_GPIOL
#define DOUTL_PORT                           GPIO_PORTL_BASE

//Digital Outputs for Port K
#define DOUTK_PERIPH                         SYSCTL_PERIPH_GPIOK
#define DOUTK_PORT                           GPIO_PORTK_BASE

//Digital Outputs for Port B
#define DOUTB_PERIPH                         SYSCTL_PERIPH_GPIOB
#define DOUTB_PORT                           GPIO_PORTB_BASE


//Non HP Digital Output Pins

//Emitter must be connected to ground and Pull Up resistor to Vcc
#define DOUT0_PIN       GPIO_PIN_5  //PL5   T0CCP1 - Timer
#define DOUT1_PIN       GPIO_PIN_3  //PB3   T5CCP1 - Timer
#define DOUT2_PIN       GPIO_PIN_2  //PB2   T5CCP0 - Timer
#define DOUT3_PIN       GPIO_PIN_4  //PL4   T0CCP0 - Timer
#define DOUT4_PIN       GPIO_PIN_6  //PK6
#define DOUT5_PIN       GPIO_PIN_7  //PK7

//HP Digital Output Pins

//D0 and D1 on the board both need to be Vcc
#define DHP0_PIN        GPIO_PIN_0  //PN0
#define DHP1_PIN        GPIO_PIN_1  //PN1
#define DHP2_PIN        GPIO_PIN_2  //PN2
#define DHP3_PIN        GPIO_PIN_3  //PN3
/**
  * @}
  */

/* Public Constants ***********************************************************/
/** @defgroup _das_d_outputs_Public_Constants Public Constants
  * @{
  */

#define LOGIC_MODE_LOW       0
#define LOGIC_MODE_HIGH      1
#define PWM_MODE             2
#define SAFETY_MODE_LOW      3
#define SAFETY_MODE_HIGH     4

/**
  * @}
  */

/* Public Structs *************************************************************/
/** @defgroup _das_d_outputs_Public_Structs Public Structs
  * @{
  */

typedef struct
{
    uint8_t     u8_DigitalOutput;
    uint8_t     u8_Mode;
    uint16_t    u16_Value;
    uint8_t     u8_DutyCycle;
    bool        b_Update;
    uint8_t     u8_SafetyVal;


}s_DOut_Table;

/**
  * @}
  */

/* Public Enums ***************************************************************/
/** @defgroup _das_d_outputs_Public_Enums Public Enums
  * @{
  */

/**
  * @}
  */

/* Prototypes *****************************************************************/
/** @defgroup _das_d_outputs_Prototypes Prototypes
  * @{
  */
static void Digital_Pin_Config();
static void General_PWM_Init();
static void Dout_CCP_Init(uint8_t u8_Pin);
static void General_PWM_Init(uint32_t u32_Base, uint32_t u32_Timer);
static void Disable_PWM(uint8_t u8_Pin);
static void Safety_Mode_Change(uint8_t u8_Pin, uint8_t u8_State);
static void Logic_Mode_Change(uint8_t u8_Pin, uint8_t u8_State);
static void PWM_Mode_Change(uint8_t u8_Pin, uint16_t u16_Freq, uint16_t u16_DutyCycle);

void Digital_Outputs_Init();
void General_Pin_Change(uint8_t u8_Pin);
void DOut_Pin_Handler();
void DOut_PWM_Mode_Set(uint8_t u8_Pin);
void DOut_Logic_Mode_Set(uint8_t u8_Pin, uint8_t u8_State);
void DOut_Frequency_Set(uint8_t u8_Pin, uint16_t u16_Freq);
void DOut_DutyCycle_Set(uint8_t u8_Pin, uint8_t u8_Duty);
void DOut_Safety_Value_Set(uint8_t u8_Pin, uint8_t u8_Value);
void DOut_Safety_Mode_Set(uint8_t u8_Pin, uint8_t u8_State);
void DOut_Safety_Trigger(uint8_t * u8_Channel);

void DOut_Test0();
void DOut_Test1();
void DOut_Test2();
void DOut_Test3();
void DOut_Test4();
void DOut_Test5();
void DOut_Test6();
void DOut_Test7();

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

#endif /* HDR_DAS_D_OUTPUTS_H_ */
