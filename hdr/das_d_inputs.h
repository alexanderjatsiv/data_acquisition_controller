/******************************************************************************/
/**
  @file          das_d_inputs.h
  @brief         Description of this file (usually copied from the module).
  */                 
/*  Revision: */
/*  Jun 16, 2020        Rev/Sprint  Initials  Remarks */ 
/******************************************************************************/
#ifndef HDR_DAS_D_INPUTS_H_
#define HDR_DAS_D_INPUTS_H_

/* Includes *******************************************************************/

/** @defgroup _das_d_inputs das_d_inputs
  * @brief Describe module
  * @{
  */  

#include "hdr/global.h"

#include "driverlib/timer.h"
#include "driverlib/interrupt.h"


#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "inc/hw_timer.h"
#include "inc/tm4c1294ncpdt.h"

/** @defgroup _das_d_inputs_Header Header
  * @{
  */  

/* Public Macros **************************************************************/
/** @defgroup _das_d_inputs_Public_Macros Public Macros
  * @{
  */

//TIMER2A Counter Register
#define TIMER2A_VAL_REG                     TIMER2_TAV_R

//TIMER2B Counter Register
#define TIMER2B_VAL_REG                     TIMER2_TBV_R

//TIMER3A Counter Register
#define TIMER3A_VAL_REG                     TIMER3_TAV_R

//TIMER3B Counter Register
#define TIMER3B_VAL_REG                     TIMER3_TBV_R

//Digital Inputs for Port M
#define DINM_PERIPH                         SYSCTL_PERIPH_GPIOM
#define DINM_PORT                           GPIO_PORTM_BASE

//PORTM Pins
#define DIN0_PIN                            GPIO_PIN_0  //Has CCP
#define DIN1_PIN                            GPIO_PIN_1  //Has CCP
#define DIN2_PIN                            GPIO_PIN_2  //Has CCP
#define DIN3_PIN                            GPIO_PIN_3  //Has CCP

//Digital Input for Port Q
#define DINQ_PERIPH                         SYSCTL_PERIPH_GPIOQ
#define DINQ_PORT                           GPIO_PORTQ_BASE

//PORTQ Pins
#define DIN4_PIN                            GPIO_PIN_1
#define DIN5_PIN                            GPIO_PIN_4
/**
  * @}
  */

/* Public Constants ***********************************************************/
/** @defgroup _das_d_inputs_Public_Constants Public Constants
  * @{
  */

//Digital Input Functions
#define LOGIC_MODE      0
#define CCP_MODE        1

//Safety Functions for CCP_MODE
#define SAFETY_HIGH     0
#define SAFETY_LOW      1
#define SAFETY_WINDOW   2
#define SAFETY_NONE     3


/**
  * @}
  */

/* Public Structs *************************************************************/
/** @defgroup _das_d_inputs_Public_Structs Public Structs
  * @{
  */
typedef struct
{
    uint8_t     u8_DigitalInput;
    uint8_t     u8_PinNo;
    uint8_t     u8_Mode;
    uint16_t    u16_Value;
    uint16_t    u16_SafetyHigh;
    uint16_t    u16_SafetyLow;
    uint8_t     u8_SafetyMode;
    uint8_t     u8_Channel[4];

}s_DIn_Table;





/**
  * @}
  */

/* Public Enums ***************************************************************/
/** @defgroup _das_d_inputs_Public_Enums Public Enums
  * @{
  */

/**
  * @}
  */

/* Prototypes *****************************************************************/
/** @defgroup _das_d_inputs_Prototypes Prototypes
  * @{
  */

//General Initializers
void Digital_Inputs_Init(void);
void Timer0_Init();
void Timer4_Init();

//Reading Functions
void Digital_Readall();

//Changing Mode Functions
void Digital_Mode(uint8_t u8_Pin);
void Frequency_Mode(uint8_t u8_Pin);

//Safety Functions
void DIn_Safety_Check(uint8_t u8_Pin, uint16_t u16_Value);
void DIn_Safety_Update(uint8_t u8_Pin, uint8_t u8_Mode, uint16_t u16_Low, uint16_t u16_High, uint8_t * u8_channel);

//DIn Testing Functions
void DInTest0();
void DInTest1();
void DInTest2();
void DInTest3();

//Static helper functions
static void Digital_Read(uint8_t u8_Pin);
static void Frequency_Read(uint8_t u8_Pins);
static void Timer_Enable_CCP(uint8_t u8_Pins);
static void Timer_Disable_CCP(uint8_t u8_Pins);
static void Frequency_Init(uint8_t u8_Pin);
static void General_Edge_Timer_Init(uint32_t u32_Periph, uint32_t u32_Base, uint32_t u32_Timer);

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

#endif /* HDR_DAS_D_INPUTS_H_ */
