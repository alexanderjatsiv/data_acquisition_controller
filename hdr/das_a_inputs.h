/******************************************************************************/
/**
  @file          das_a_inputs.h
  @brief         Description of this file (usually copied from the module).
  */                 
/*  Revision: */
/*  May 22, 2020        Rev/Sprint  Initials  Remarks */ 
/******************************************************************************/
#ifndef HDR_DAS_A_INPUTS_H_
#define HDR_DAS_A_INPUTS_H_

/* Includes *******************************************************************/

#include "hdr/global.h"
#include "hdr/das_d_outputs.h"

#include "driverlib/adc.h"
#include "driverlib/interrupt.h"
#include "driverlib/udma.h"
#include "driverlib/timer.h"

#include "inc/hw_adc.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"

/** @defgroup _das_a_inputs das_a_inputs
  * @brief Describe module
  * @{
  */  

/** @defgroup _das_a_inputs_Header Header
  * @{
  */  

/* Public Macros **************************************************************/
/** @defgroup _das_a_inputs_Public_Macros Public Macros
  * @{
  */



//ADC Indices
#define AIHV0                   0
#define AIHV1                   1
#define AIHV2                   2
#define AIHV3                   3
#define AIHV4                   4
#define AIHV5                   5
#define AIHV6                   6
#define AIHV7                   7
#define AIHV8                   8
#define AIHV9                   9
#define AIHV10                  10
#define AIHV11                  11
#define AILV0                   12
#define AILV1                   13
#define AILV2                   14
#define AILV3                   15

//Enable peripherals
#define AIHV_PERIPH0                    SYSCTL_PERIPH_GPIOD     //Enable AIN HV0-HV7
#define AIHV_PERIPH1                    SYSCTL_PERIPH_GPIOK     //Enable AIN HV8-HV11
#define AILV_PERIPH                     SYSCTL_PERIPH_GPIOE     //Enable AIN LV0-LV3

//AIN ports
#define AIHV_PORT0                      GPIO_PORTD_BASE         //Port for AIN HV0-HV7
#define AIHV_PORT1                      GPIO_PORTK_BASE         //Port for AIN HV8-HV11
#define AILV_PORT                       GPIO_PORTE_BASE         //Port for AIN LV0-LV3

//AIN pins
#define AIHV0_PIN                       GPIO_PIN_0              //PD0
#define AIHV1_PIN                       GPIO_PIN_2              //PD2
#define AIHV2_PIN                       GPIO_PIN_1              //PD1
#define AIHV3_PIN                       GPIO_PIN_3              //PD3
#define AIHV4_PIN                       GPIO_PIN_6              //PD6
#define AIHV5_PIN                       GPIO_PIN_7              //PD7
#define AIHV6_PIN                       GPIO_PIN_4              //PD4
#define AIHV7_PIN                       GPIO_PIN_5              //PD5
#define AIHV8_PIN                       GPIO_PIN_3              //PK3
#define AIHV9_PIN                       GPIO_PIN_1              //PK1
#define AIHV10_PIN                      GPIO_PIN_2              //PK2
#define AIHV11_PIN                      GPIO_PIN_0              //PK0
#define AILV0_PIN                       GPIO_PIN_3              //PE3
#define AILV1_PIN                       GPIO_PIN_2              //PE2
#define AILV2_PIN                       GPIO_PIN_1              //PE1
#define AILV3_PIN                       GPIO_PIN_0              //PE0

//ADC Channels

//HV Pin Channels
#define AIHV0_ADC                       ADC_CTL_CH15
#define AIHV1_ADC                       ADC_CTL_CH13
#define AIHV2_ADC                       ADC_CTL_CH14
#define AIHV3_ADC                       ADC_CTL_CH12
#define AIHV4_ADC                       ADC_CTL_CH5
#define AIHV5_ADC                       ADC_CTL_CH4
#define AIHV6_ADC                       ADC_CTL_CH7
#define AIHV7_ADC                       ADC_CTL_CH6
#define AIHV8_ADC                       ADC_CTL_CH19
#define AIHV9_ADC                       ADC_CTL_CH17
#define AIHV10_ADC                      ADC_CTL_CH18
#define AIHV11_ADC                      ADC_CTL_CH16

//LV Pin Channels
#define AILV0_ADC                       ADC_CTL_CH0
#define AILV1_ADC                       ADC_CTL_CH1
#define AILV2_ADC                       ADC_CTL_CH2
#define AILV3_ADC                       ADC_CTL_CH3

//ADC Gate Peripherals
#define AIHV0_GATE_PERIPH               SYSCTL_PERIPH_GPIOP
#define AIHV0_GATE_PORT                 GPIO_PORTP_BASE
#define AIHV0_GATE_PIN                  GPIO_PIN_4
#define AIHV1_GATE_PERIPH               SYSCTL_PERIPH_GPIOF
#define AIHV1_GATE_PORT                 GPIO_PORTF_BASE
#define AIHV1_GATE_PIN                  GPIO_PIN_3
#define AIHV2_GATE_PERIPH               SYSCTL_PERIPH_GPIOF
#define AIHV2_GATE_PORT                 GPIO_PORTF_BASE
#define AIHV2_GATE_PIN                  GPIO_PIN_2
#define AIHV3_GATE_PERIPH               SYSCTL_PERIPH_GPIOF
#define AIHV3_GATE_PORT                 GPIO_PORTF_BASE
#define AIHV3_GATE_PIN                  GPIO_PIN_1
#define AIHV4_GATE_PERIPH               SYSCTL_PERIPH_GPIOF
#define AIHV4_GATE_PORT                 GPIO_PORTF_BASE
#define AIHV4_GATE_PIN                  GPIO_PIN_0
#define AIHV5_GATE_PERIPH               SYSCTL_PERIPH_GPIOF
#define AIHV5_GATE_PORT                 GPIO_PORTF_BASE
#define AIHV5_GATE_PIN                  GPIO_PIN_4
#define AIHV6_GATE_PERIPH               SYSCTL_PERIPH_GPIOP
#define AIHV6_GATE_PORT                 GPIO_PORTP_BASE
#define AIHV6_GATE_PIN                  GPIO_PIN_5
#define AIHV7_GATE_PERIPH               SYSCTL_PERIPH_GPIOM
#define AIHV7_GATE_PORT                 GPIO_PORTM_BASE
#define AIHV7_GATE_PIN                  GPIO_PIN_7
#define AIHV8_GATE_PERIPH               SYSCTL_PERIPH_GPIOM
#define AIHV8_GATE_PORT                 GPIO_PORTM_BASE
#define AIHV8_GATE_PIN                  GPIO_PIN_6
#define AIHV9_GATE_PERIPH               SYSCTL_PERIPH_GPIOL
#define AIHV9_GATE_PORT                 GPIO_PORTL_BASE
#define AIHV9_GATE_PIN                  GPIO_PIN_1
#define AIHV10_GATE_PERIPH              SYSCTL_PERIPH_GPIOL
#define AIHV10_GATE_PORT                GPIO_PORTL_BASE
#define AIHV10_GATE_PIN                 GPIO_PIN_2
#define AIHV11_GATE_PERIPH              SYSCTL_PERIPH_GPIOL
#define AIHV11_GATE_PORT                GPIO_PORTL_BASE
#define AIHV11_GATE_PIN                 GPIO_PIN_3

#define AILV0_GATE_PERIPH               SYSCTL_PERIPH_GPIOA
#define AILV0_GATE_PORT                 GPIO_PORTA_BASE
#define AILV0_GATE_PIN                  GPIO_PIN_5
#define AILV1_GATE_PERIPH               SYSCTL_PERIPH_GPIOE
#define AILV1_GATE_PORT                 GPIO_PORTE_BASE
#define AILV1_GATE_PIN                  GPIO_PIN_4
#define AILV2_GATE_PERIPH               SYSCTL_PERIPH_GPIOE
#define AILV2_GATE_PORT                 GPIO_PORTE_BASE
#define AILV2_GATE_PIN                  GPIO_PIN_5
#define AILV3_GATE_PERIPH               SYSCTL_PERIPH_GPIOL
#define AILV3_GATE_PORT                 GPIO_PORTL_BASE
#define AILV3_GATE_PIN                  GPIO_PIN_0

/**
  * @}
  */

/* Public Constants ***********************************************************/
/** @defgroup _das_a_inputs_Public_Constants Public Constants
  * @{
  */

#define TOTAL_ANALOG_INPUTS     16

//Safety Functions
#define SAFETY_HIGH     0
#define SAFETY_LOW      1
#define SAFETY_WINDOW   2
#define SAFETY_NONE     3

/**
  * @}
  */

/* Public Structs *************************************************************/
/** @defgroup _das_a_inputs_Public_Structs Public Structs
  * @{
  */

//Defines each of the AIN's
typedef struct
{
    uint8_t u8_name;                //Name of which ADC Pin we are reading from
    bool b_active;                  //A boolean to say if we are using this reading
    uint8_t u8_SafetyType;          //Safety Type
    uint16_t u16_SafetyLow;         //32 bit integer for the low value
    uint16_t u16_SafetyHigh;        //32 bit integer for the high value
    uint8_t u8_SafetyChannel[4];       //Array for the channels that are enabled
} s_analogInputs_t;

//Defines the Peripherals, Ports, and Pins for each AIN
typedef struct
{
    uint32_t u32_periph;
    uint32_t u32_port;
    uint32_t u32_pin;
} s_analogGates_t;

typedef struct
{
    uint32_t u32_sequence;
}s_ADCsequence;

typedef struct
{
    uint8_t u32_OversamplingRate;
    uint32_t u32_Timer1us;
}s_ADCsamplingInfo;
typedef struct
{
    s_ADCsamplingInfo s_SamplingTable;
    s_analogInputs_t s_AnalogInputsTable[16];
}s_ADCTableStruct;
/**
  * @}
  */

/* Public Enums ***************************************************************/
/** @defgroup _das_a_inputs_Public_Enums Public Enums
  * @{
  */

/**
  * @}
  */

/* Prototypes *****************************************************************/
/** @defgroup _das_a_inputs_Prototypes Prototypes
  * @{
  */
static void analog_mosfets_init(s_analogInputs_t * s_AnalogInputs);
void ADC_initializer();
static void ADC_peripheral_initializer(void);
static void ADC_sequence_step_initializer(uint8_t u8_sequencerNo, uint8_t u8_priority, uint8_t u8_length, s_ADCsequence * s_sequence);
static void uDMA_ADC_initializer();
static void ADC_Timer1_Initializer(void);
static void analog_mosfets_init(s_analogInputs_t * s_AnalogInputs);
int ADC_Timer1_Frequency(uint32_t u32_Time_us, uint8_t u8_SamplingRate);
void ADC_Mosfet_configure(uint8_t u8_ADCNo, bool b_config);
void ADC_Safety_Update(uint8_t u8_pin, uint8_t u8_Type, uint16_t u16_Low, uint16_t u16_High, uint8_t * u8_channel);
void ADC_Safety_Check(uint8_t u8_Pin, uint32_t u32_Value);
void ADC_TEST0();
void ADC_TEST1();
void ADC_TEST2();
void ADC_TEST3();
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

#endif /* HDR_DAS_A_INPUTS_H_ */
