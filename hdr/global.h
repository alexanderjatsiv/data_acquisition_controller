/******************************************************************************/
/**
  @file          global.h
  @brief         Description of this file (usually copied from the module).
  */                 
/*  Revision: */
/*  Jun 1, 2020        Rev/Sprint  Initials  Remarks */ 
/******************************************************************************/
#ifndef HDR_GLOBAL_H_
#define HDR_GLOBAL_H_

/* Includes *******************************************************************/

/** @defgroup _global global
  * @brief Describe module
  * @{
  */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "driverlib/sysctl.h"
#include "driverlib/rom_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"

#include "hdr/das_d_outputs.h"

#include "inc/hw_nvic.h"
#include "inc/tm4c1294ncpdt.h"

/** @defgroup _global_Header Header
  * @{
  */  

/* Public Macros **************************************************************/
/** @defgroup _global_Public_Macros Public Macros
  * @{
  */

/**
  * @}
  */

/* Public Constants ***********************************************************/
/** @defgroup _global_Public_Constants Public Constants
  * @{
  */
#define SYS_CLOCK_RATE   120000000UL

/**
  * @}
  */

/* Public Structs *************************************************************/
/** @defgroup _global_Public_Structs Public Structs
  * @{
  */

/**
  * @}
  */

/* Public Enums ***************************************************************/
/** @defgroup _global_Public_Enums Public Enums
  * @{
  */

/**
  * @}
  */

/* Prototypes *****************************************************************/
/** @defgroup _global_Prototypes Prototypes
  * @{
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

#endif /* HDR_GLOBAL_H_ */
