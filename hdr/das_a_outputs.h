/******************************************************************************/
/**
  @file          das_a_outputs.h
  @brief         Description of this file (usually copied from the module).
  */                 
/*  Revision: */
/*  Jun 4, 2020        Rev/Sprint  Initials  Remarks */ 
/******************************************************************************/
#ifndef HDR_DAS_A_OUTPUTS_H_
#define HDR_DAS_A_OUTPUTS_H_

/* Includes *******************************************************************/

/** @defgroup _das_a_outputs das_a_outputs
  * @brief Describe module
  * @{
  */  

#include "hdr/global.h"

#include "driverlib/ssi.h"

#include "inc/hw_ssi.h"
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"

/** @defgroup _das_a_outputs_Header Header
  * @{
  */  

/* Public Macros **************************************************************/
/** @defgroup _das_a_outputs_Public_Macros Public Macros
  * @{
  */

/*
 * SSI Ports
 */
#define DAC_SSI_BASE        SSI0_BASE
#define DAC_SSI_SYS_PERIPH  SYSCTL_PERIPH_SSI0

/*
 * GPIO for SSI Pins
 */
#define DAC_GPIO_PORT_BASE        GPIO_PORTA_BASE   //Pins are on port A
#define DAC_GPIO_SYSCTL_PERIPH    SYSCTL_PERIPH_GPIOA   //Periph for port A
#define DAC_SSI_CLK               GPIO_PIN_2    //Clock is on PA2
#define DAC_SSI_FSS               GPIO_PIN_3    //Frame signal is on PA3
#define DAC_SSI_TX                GPIO_PIN_4    //DAT0 is on PA4
#define DAC_SSI_PINS              (DAC_SSI_TX | DAC_SSI_CLK | DAC_SSI_FSS)

/**
  * @}
  */

/* Public Constants ***********************************************************/
/** @defgroup _das_a_outputs_Public_Constants Public Constants
  * @{
  */
//DAC outputs
#define DAC0               0x00
#define DAC1               0x4000
#define DAC2               0x8000
#define DAC3               0xC000

//DAC Modes
#define WR_REG_UPDATE      0x1000 /*Write to specified register and update outputs */
#define WR_REG_NO_UPDATE   0x00   /*Write to specified register and do not update outputs */
#define WR_REG_ALL_UPDATE  0x2000 /*Write to all registers and update outputs */
#define POWER_DOWN         0x3000 /*Power-down outputs */

//Power Down Modes
#define High_Z_outputs     0x00   /* Causes the outputs to have a high impedance state */


/**
  * @}
  */

/* Public Structs *************************************************************/
/** @defgroup _das_a_outputs_Public_Structs Public Structs
  * @{
  */
typedef struct
{
    uint16_t    u16_AnalogOutput;
    bool        b_State;
    uint16_t    u16_Output;
}s_AnalogOutputs;

/**
  * @}
  */

/* Public Enums ***************************************************************/
/** @defgroup _das_a_outputs_Public_Enums Public Enums
  * @{
  */

/**
  * @}
  */

/* Prototypes *****************************************************************/
/** @defgroup _das_a_outputs_Prototypes Prototypes
  * @{
  */

void Analog_Output_TEST0();
static void SSI_Config_Init();
static void DAC_Write(uint32_t u32_Data);
void Analog_Output_Switch(uint8_t u8_channel, bool b_Switch);
void Analog_Output_Initializer();
void Analog_Output_Set(uint8_t u8_Channel, uint16_t u16_OutputValue);
void Analog_Output_TEST1();
void Analog_Output_TEST2();
void Analog_Output_TEST3();

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

#endif /* HDR_DAS_A_OUTPUTS_H_ */
