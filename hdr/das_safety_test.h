/******************************************************************************/
/**
  @file          das_safety_test.h
  @brief         Description of this file (usually copied from the module).
  */                 
/*  Revision: */
/*  Jul 2, 2020        Rev/Sprint  Initials  Remarks */ 
/******************************************************************************/
#ifndef HDR_DAS_SAFETY_TEST_H_
#define HDR_DAS_SAFETY_TEST_H_

/* Includes *******************************************************************/


#include "hdr/global.h"
#include "hdr/das_a_inputs.h"
#include "hdr/das_a_outputs.h"
#include "hdr/das_d_inputs.h"
#include "hdr/das_systick.h"
#include "hdr/das_d_outputs.h"

/** @defgroup _das_safety_test das_safety_test
  * @brief Describe module
  * @{
  */  

/** @defgroup _das_safety_test_Header Header
  * @{
  */  

/* Public Macros **************************************************************/
/** @defgroup _das_safety_test_Public_Macros Public Macros
  * @{
  */

/**
  * @}
  */

/* Public Constants ***********************************************************/
/** @defgroup _das_safety_test_Public_Constants Public Constants
  * @{
  */

/**
  * @}
  */

/* Public Structs *************************************************************/
/** @defgroup _das_safety_test_Public_Structs Public Structs
  * @{
  */

/**
  * @}
  */

/* Public Enums ***************************************************************/
/** @defgroup _das_safety_test_Public_Enums Public Enums
  * @{
  */

/**
  * @}
  */

/* Prototypes *****************************************************************/
/** @defgroup _das_safety_test_Prototypes Prototypes
  * @{
  */

void Safety_Test_Digital_Logical();
void Logical_Test_Handler();
void Safety_Test_Digital_Frequency();
void Frequency_Test_Handler();
void Safety_Test_Analog();
void AnalogInput_Test_Handler();

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

#endif /* HDR_DAS_SAFETY_TEST_H_ */
