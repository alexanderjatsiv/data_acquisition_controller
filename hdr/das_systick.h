/******************************************************************************/
/**
  @file          das_systick.h
  @brief         Description of this file (usually copied from the module).
  */                 
/*  Revision: */
/*  Jun 25, 2020        Rev/Sprint  Initials  Remarks */ 
/******************************************************************************/
#ifndef HDR_DAS_SYSTICK_H_
#define HDR_DAS_SYSTICK_H_

/* Includes *******************************************************************/

/** @defgroup _das_systick das_systick
  * @brief Describe module
  * @{
  */  

#include "hdr/global.h"
#include "hdr/das_d_inputs.h"
#include "hdr/das_safety_test.h"

#include "driverlib/systick.h"

#include "inc/hw_nvic.h"
#include "inc/tm4c1294ncpdt.h"

/** @defgroup _das_systick_Header Header
  * @{
  */  

/* Public Macros **************************************************************/
/** @defgroup _das_systick_Public_Macros Public Macros
  * @{
  */

/**
  * @}
  */

/* Public Constants ***********************************************************/
/** @defgroup _das_systick_Public_Constants Public Constants
  * @{
  */
#define EVENT_ENABLED   true
#define EVENT_DISABLED  false

#define EVENT_TRIGGER   true
#define EVENT_RESET     false

/**
  * @}
  */

/* Public Structs *************************************************************/
/** @defgroup _das_systick_Public_Structs Public Structs
  * @{
  */

typedef struct
{
    uint16_t    u16_EventTimerms;
    uint16_t    u16_CurTimems;
    bool        b_Event;
    bool        b_Enabled;
    void    (*function)(void);
}s_Event;

/**
  * @}
  */

/* Public Enums ***************************************************************/
/** @defgroup _das_systick_Public_Enums Public Enums
  * @{
  */

/**
  * @}
  */

/* Prototypes *****************************************************************/
/** @defgroup _das_systick_Prototypes Prototypes
  * @{
  */

void sys_tick_handler(void);
void Event_Handler(void);
void Sys_Tick_Init(void);

static void Event_Increment(void);
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

#endif /* HDR_DAS_SYSTICK_H_ */
