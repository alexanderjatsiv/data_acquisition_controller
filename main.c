/******************************************************************************/
/**
  * @file           das_a_inputs
  * @brief          Reads analog inputs from microprocessor
  */
/*  Dependencies: List all dependencies of this module. */

/*  Revision: */
/*  May 22, 2020        Rev/Sprint  Initials  Remarks */ 
/******************************************************************************/
/** @addtogroup _main
  * @{
  */ 

/** @defgroup _main_Source Source
  * @{
  */

/* Includes *******************************************************************/

#include "hdr/global.h"
#include "hdr/das_a_inputs.h"
#include "hdr/das_a_outputs.h"
#include "hdr/das_d_inputs.h"
#include "hdr/das_systick.h"
#include "hdr/das_d_outputs.h"
#include "hdr/das_safety_test.h"
/* Private Macros *************************************************************/
/** @defgroup _main_Private_Macros Private Macros
  * @{
  */

/**
  * @}
  */

/* Private Constants **********************************************************/
/** @defgroup _main_Private_Constants Private Constants
  * @{
  */

/**
  * @}
  */

/* Private Variables **********************************************************/
/** @defgroup _main_Private_Variables Private Variables
  * @{
  */

/**
 * Control Table for the uDMA
 */
#pragma DATA_ALIGN(u8_uDMAControlBase, 1024)
uint8_t u8_uDMAControlBase[1024];

/**
  * @}
  */

/* Private functions **********************************************************/
/** @defgroup _main_Private_Functions Private Functions
  * @{
  */

/*  Function:   functionName */
/**
  *  @brief     Function description
  *  @param     Function parameters
  *  @retval    Return value
  */


/**
  * @}
  */

/* Public functions ***********************************************************/
/** @defgroup _main_Public_Functions Public Functions
  * @{
  */


/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */


int main(void)
{
    //Configure system clock
    MAP_SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ | //Sets the frequency of the external oscillator
                            SYSCTL_OSC_MAIN |   //System uses external oscillator
                            SYSCTL_USE_PLL  |   //Uses PLL as system clock
                            SYSCTL_CFG_VCO_480),//Sets PLL output to 480MHz
                            SYS_CLOCK_RATE);    //Attempts to set the clock rate to SYS_CLOCK_RATE - 120MHz

    //Configures the alternate peripheral clock source
    MAP_SysCtlAltClkConfig(SYSCTL_ALTCLK_PIOSC);    //Uses the PIOSC as the alternate clock source

    //Enable the UDMA peripheral
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);
    //Enable the UDMA
    MAP_uDMAEnable();
    //Sets the base address for the channel control table
    MAP_uDMAControlBaseSet(u8_uDMAControlBase);

    Sys_Tick_Init();

    Safety_Test_Digital_Frequency();
    Safety_Test_Analog();
    while(1)
    {
        Event_Handler();
    }

}
