/******************************************************************************/
/**
  * @file           das_d_outputs.c
  * @brief          Description of this file.
  */
/*  Dependencies: List all dependencies of this module. */

/*  Revision: */
/*  Jun 29, 2020        Rev/Sprint  Initials  Remarks */ 
/******************************************************************************/
/** @addtogroup _das_d_outputs
  * @{
  */ 

/** @defgroup _das_d_outputs_Source Source
  * @{
  */

/* Includes *******************************************************************/

#include "hdr/das_d_outputs.h"

/* Private Macros *************************************************************/
/** @defgroup _das_d_outputs_Private_Macros Private Macros
  * @{
  */

/**
  * @}
  */

/* Private Constants **********************************************************/
/** @defgroup _das_d_outputs_Private_Constants Private Constants
  * @{
  */

/**
  * @}
  */

/* Private Variables **********************************************************/
/** @defgroup _das_d_outputs_Private_Variables Private Variables
  * @{
  */
s_DOut_Table s_DOutTable[10] =
{
     {DOUT0_PIN, LOGIC_MODE_LOW, 5000, 50, false, 0},
     {DOUT1_PIN, LOGIC_MODE_LOW, 5000, 50, false, 0},
     {DOUT2_PIN, LOGIC_MODE_LOW, 5000, 50, false, 0},
     {DOUT3_PIN, LOGIC_MODE_LOW, 5000, 50, false, 0},
     {DOUT4_PIN, LOGIC_MODE_LOW, 5000, 50, false, 0},
     {DOUT5_PIN, LOGIC_MODE_LOW, 5000, 50, false, 0},
     {DHP0_PIN, LOGIC_MODE_LOW, 0, 0, false, 0},
     {DHP1_PIN, LOGIC_MODE_LOW, 0, 0, false, 0},
     {DHP2_PIN, LOGIC_MODE_LOW, 0, 0, false, 0},
     {DHP3_PIN, LOGIC_MODE_LOW, 0, 0, false, 0}
};

/**
  * @}
  */

/* Private functions **********************************************************/
/** @defgroup _das_d_outputs_Private_Functions Private Functions
  * @{
  */

/*
 * Function: Dout_CCP_Init()
 *
 * @brief       Initializes the CCP pin configurations
 * @param       u8_Pin ---> The Pin to initialize
 * @retval      No return value
 */
static void Dout_CCP_Init(uint8_t u8_Pin)
{
    switch(u8_Pin)
    {
    case 0:
        MAP_GPIOPinConfigure(GPIO_PL5_T0CCP1);
        MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
        General_PWM_Init(TIMER0_BASE, TIMER_B);
        break;

    case 1:
        MAP_GPIOPinConfigure(GPIO_PB3_T5CCP1);
        MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER5);
        General_PWM_Init(TIMER5_BASE, TIMER_B);
        break;

    case 2:
        MAP_GPIOPinConfigure(GPIO_PB2_T5CCP0);
        MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER5);
        General_PWM_Init(TIMER5_BASE, TIMER_A);
        break;

    case 3:
        MAP_GPIOPinConfigure(GPIO_PL4_T0CCP0);
        MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
        General_PWM_Init(TIMER0_BASE, TIMER_A);
        break;

    default:
        break;

    }
}

/*
 * Function: General_PWM_Init()
 *
 * @brief       Initializes the Timer Configurations
 * @param       No parameters
 * @retval      No return value
 */
static void General_PWM_Init(uint32_t u32_Base, uint32_t u32_Timer)
{
    //Configure the Timer to 16 bit mode and PWM
    MAP_TimerConfigure(u32_Base, TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PWM | TIMER_CFG_B_PWM);

    //Set the clock to be set with the system clock
    MAP_TimerClockSourceSet(u32_Base, TIMER_CLOCK_PIOSC);

    //Load the Frequency in here
    MAP_TimerLoadSet(u32_Base, u32_Timer, 0xFFFF);

    //Load the duty cycle in here
    MAP_TimerMatchSet(u32_Base, u32_Timer, 0x8000);
}
/*
 * Function: Digital_Pin_Config()
 *
 * @brief       Initializes the digital output pins
 * @param       No parameters
 * @retval      No return value
 */
static void Digital_Pin_Config()
{
    //Enable Port L Pins
    MAP_SysCtlPeripheralEnable(DOUTL_PERIPH);
    MAP_GPIOPinTypeGPIOOutput(DOUTL_PORT, DOUT0_PIN | DOUT3_PIN);
    MAP_GPIOPadConfigSet(DOUTL_PORT, DOUT0_PIN | DOUT3_PIN, GPIO_STRENGTH_12MA, GPIO_PIN_TYPE_STD);
    MAP_GPIOPinWrite(DOUTL_PORT, DOUT0_PIN | DOUT3_PIN, DOUT0_PIN | DOUT3_PIN);


    //Enable Port K Pins
    MAP_SysCtlPeripheralEnable(DOUTK_PERIPH);
    MAP_GPIOPinTypeGPIOOutput(DOUTK_PORT, DOUT4_PIN | DOUT5_PIN);
    MAP_GPIOPadConfigSet(DOUTK_PORT, DOUT4_PIN | DOUT5_PIN, GPIO_STRENGTH_12MA, GPIO_PIN_TYPE_STD);
    MAP_GPIOPinWrite(DOUTK_PORT, DOUT4_PIN | DOUT5_PIN, DOUT4_PIN | DOUT5_PIN);

    //Enable Port B Pins
    MAP_SysCtlPeripheralEnable(DOUTB_PERIPH);
    MAP_GPIOPinTypeGPIOOutput(DOUTB_PORT, DOUT1_PIN | DOUT2_PIN);
    MAP_GPIOPadConfigSet(DOUTB_PORT, DOUT1_PIN | DOUT2_PIN, GPIO_STRENGTH_12MA, GPIO_PIN_TYPE_STD);
    MAP_GPIOPinWrite(DOUTB_PORT, DOUT1_PIN | DOUT2_PIN, DOUT1_PIN | DOUT2_PIN);

    //Enable Port N Pins
    MAP_SysCtlPeripheralEnable(DOUTN_PERIPH);
    MAP_GPIOPinTypeGPIOOutput(DOUTN_PORT, DHP0_PIN | DHP1_PIN | DHP2_PIN | DHP3_PIN);
    MAP_GPIOPadConfigSet(DOUTN_PORT, DHP0_PIN | DHP1_PIN | DHP2_PIN | DHP3_PIN, GPIO_STRENGTH_8MA, GPIO_PIN_TYPE_STD);
    MAP_GPIOPinWrite(DOUTN_PORT, DHP0_PIN | DHP1_PIN | DHP2_PIN | DHP3_PIN, 0);
}

/*
 * Function: Disable_PWM()
 *
 * @brief       Disable any of the PWM Timers
 * @param       u8_Pin --> Pin to change
 * @retval      No return value
 */
static void Disable_PWM(uint8_t u8_Pin)
{
    switch(u8_Pin)
    {
    case 0:
        MAP_TimerDisable(TIMER0_BASE, TIMER_B);
        break;
    case 1:
        MAP_TimerDisable(TIMER5_BASE, TIMER_B);
        break;
    case 2:
        MAP_TimerDisable(TIMER5_BASE, TIMER_A);
        break;
    case 3:
        MAP_TimerDisable(TIMER0_BASE, TIMER_A);
        break;
    }
}

/*
 * Function: Safety_Mode_Change()
 *
 * @brief       Changes any of the HPDO to Safety Mode
 * @param       u8_Pin ---> Pin to change
 *              u8_State ---> The state to change to
 * @retval      No return value
 */
static void Safety_Mode_Change(uint8_t u8_Pin, uint8_t u8_State)
{
    MAP_GPIOPinWrite(DOUTN_PORT, s_DOutTable[u8_Pin].u8_DigitalOutput, u8_State);
}

/*
 * Function: Logic_Mode_Change()
 *
 * @brief       Changes any of the Digital Output pins to Logic Mode
 * @param       u8_Pin ---> Pin to change
 *              u8_State ---> state to change to
 * @retval      No return value
 */
static void Logic_Mode_Change(uint8_t u8_Pin, uint8_t u8_State)
{
    if((u8_Pin == 0) || (u8_Pin == 3))
    {
        MAP_GPIOPinWrite(DOUTL_PORT, s_DOutTable[u8_Pin].u8_DigitalOutput, u8_State);
    }
    else if((u8_Pin == 1) || (u8_Pin == 2))
    {
        MAP_GPIOPinWrite(DOUTB_PORT, s_DOutTable[u8_Pin].u8_DigitalOutput, u8_State);
    }
    else if((u8_Pin == 4) || (u8_Pin == 5))
    {
        MAP_GPIOPinWrite(DOUTK_PORT, s_DOutTable[u8_Pin].u8_DigitalOutput, u8_State);
    }
    else if((u8_Pin > 5) && (u8_Pin < 10))
    {
        MAP_GPIOPinWrite(DOUTN_PORT, s_DOutTable[u8_Pin].u8_DigitalOutput, u8_State);
    }
}

/*
 * Function: PWM_Mode_Change()
 *
 * @brief       Changes the CCP pins to a PWM wave
 * @param       u8_Pin ---> Pin to change
 *              u16_Freq ---> Frequency to set for the PWM
 *              u16_DutyCycle ---> DutyCycle to set for the PWM
 * @retval      No return value
 */
static void PWM_Mode_Change(uint8_t u8_Pin, uint16_t u16_Freq, uint16_t u16_DutyCycle)
{
    switch(u8_Pin)
    {
    case 0:
        MAP_GPIOPinTypeTimer(DOUTL_PORT, DOUT0_PIN);
        MAP_TimerDisable(TIMER0_BASE, TIMER_B);
        MAP_TimerLoadSet(TIMER0_BASE, TIMER_B, u16_Freq);
        MAP_TimerMatchSet(TIMER0_BASE, TIMER_B, u16_DutyCycle);
        MAP_TimerEnable(TIMER0_BASE, TIMER_B);
        break;

    case 1:
        MAP_GPIOPinTypeTimer(DOUTB_PORT, DOUT1_PIN);
        MAP_TimerDisable(TIMER5_BASE, TIMER_B);
        MAP_TimerLoadSet(TIMER5_BASE, TIMER_B, u16_Freq);
        MAP_TimerMatchSet(TIMER5_BASE, TIMER_B, u16_DutyCycle);
        MAP_TimerEnable(TIMER5_BASE, TIMER_B);
        break;

    case 2:
        MAP_GPIOPinTypeTimer(DOUTB_PORT, DOUT2_PIN);
        MAP_TimerDisable(TIMER5_BASE, TIMER_A);
        MAP_TimerLoadSet(TIMER5_BASE, TIMER_A, u16_Freq);
        MAP_TimerMatchSet(TIMER5_BASE, TIMER_A, u16_DutyCycle);
        MAP_TimerEnable(TIMER5_BASE, TIMER_A);
        break;

    case 3:
        MAP_GPIOPinTypeTimer(DOUTL_PORT, DOUT3_PIN);
        MAP_TimerDisable(TIMER0_BASE, TIMER_A);
        MAP_TimerLoadSet(TIMER0_BASE, TIMER_A, u16_Freq);
        MAP_TimerMatchSet(TIMER0_BASE, TIMER_A, u16_DutyCycle);
        MAP_TimerEnable(TIMER0_BASE, TIMER_A);
        break;

    default:
        break;
    }
}
/**
  * @}
  */

/* Public functions ***********************************************************/
/** @defgroup _das_d_outputs_Public_Functions Public Functions
  * @{
  */

/*
 * Function: DOut_Test0()
 *
 * @brief       Function to test the Code
 * @param       No parameters
 * @retval      No return value
 */
void DOut_Test0()
{
    Digital_Outputs_Init();

    DOut_PWM_Mode_Set(0);
    DOut_PWM_Mode_Set(1);
    DOut_PWM_Mode_Set(2);
    DOut_PWM_Mode_Set(3);


    int i;
    for(i = 4; i < 10; i++)
    {
        DOut_Logic_Mode_Set(i, 1);
    }


}

/*
 * Function: DOut_Test1()
 *
 * @brief       Function to test the Code
 * @param       No parameters
 * @retval      No return value
 */
void DOut_Test1()
{
    Digital_Outputs_Init();

    DOut_PWM_Mode_Set(0);
    DOut_PWM_Mode_Set(1);
    DOut_PWM_Mode_Set(2);
    DOut_PWM_Mode_Set(3);


    DOut_Frequency_Set(0, 1250);
    DOut_Frequency_Set(1, 1000);
    DOut_Frequency_Set(2, 750);
    DOut_Frequency_Set(3, 500);

    DOut_DutyCycle_Set(0, 95);
    DOut_DutyCycle_Set(1, 95);
    DOut_DutyCycle_Set(2, 95);
    DOut_DutyCycle_Set(3, 95);
    int i;
    for(i = 4; i < 10; i++)
    {
        DOut_Logic_Mode_Set(i, 1);
    }


}

/*
 * Function: DOut_Test2()
 *
 * @brief       Function to test the Code
 * @param       No parameters
 * @retval      No return value
 */
void DOut_Test2()
{
    Digital_Outputs_Init();


    uint8_t u8_test;
    for(u8_test = 0; u8_test < 10; u8_test++)
    {
        DOut_Logic_Mode_Set(u8_test, 1); //Set all outputs to high
    }


}

/*
 * Function: DOut_Test3()
 *
 * @brief       Function to test the Code
 * @param       No parameters
 * @retval      No return value
 */
void DOut_Test3()
{
    Digital_Outputs_Init();


    uint8_t u8_test;
    for(u8_test = 0; u8_test < 10; u8_test++)
    {
        DOut_Logic_Mode_Set(u8_test, 0); //Set all outputs to low
    }


}

/*
 * Function: DOut_Test4()
 *
 * @brief       Function to test the Code
 * @param       No parameters
 * @retval      No return value
 */
void DOut_Test4()
{
    Digital_Outputs_Init();


    uint8_t u8_test;
    for(u8_test = 0; u8_test < 10; u8_test = u8_test + 2)
    {
        DOut_Logic_Mode_Set(u8_test, 1); //Set all other outputs to high
    }


}

/*
 * Function: DOut_Test5()
 *
 * @brief       Function to test the Code
 * @param       No parameters
 * @retval      No return value
 */
void DOut_Test5()
{
    Digital_Outputs_Init();


    uint8_t u8_test;
    for(u8_test = 0; u8_test < 6; u8_test++)
    {
        DOut_Logic_Mode_Set(u8_test, 0); //Set all other outputs to high
    }

    for(u8_test = 6; u8_test < 10; u8_test++)
    {
        DOut_Safety_Mode_Set(u8_test, 1); //Set safeties to high
    }
}

/*
 * Function: DOut_Test6()
 *
 * @brief       Function to test the Code
 * @param       No parameters
 * @retval      No return value
 */
void DOut_Test6()
{
    Digital_Outputs_Init();


    uint8_t u8_test;
    for(u8_test = 0; u8_test < 6; u8_test++)
    {
        DOut_Logic_Mode_Set(u8_test, 0); //Set all other outputs to high
    }

    for(u8_test = 6; u8_test < 10; u8_test++)
    {
        DOut_Safety_Mode_Set(u8_test, 0); //Set safeties to low
    }
}

/*
 * Function: DOut_Test7()
 *
 * @brief       Function to test the Code
 * @param       No parameters
 * @retval      No return value
 */
void DOut_Test7()
{
    Digital_Outputs_Init();


    uint8_t u8_test;
    for(u8_test = 0; u8_test < 6; u8_test++)
    {
        DOut_Logic_Mode_Set(u8_test, 0); //Set all other outputs to high
    }

    for(u8_test = 6; u8_test < 10; u8_test = u8_test + 2)
    {
        DOut_Safety_Mode_Set(u8_test, 1); //Set safeties to high
    }
}

/*
 * Function: DOut_Safety_Trigger()
 *
 * @brief       Function to be called to change a pin's safety value
 * @param       u8_Pin ---> Pin to Change the Safety Value
 *              u8_Value ---> Tells if the output is either High or Low
 * @retval      No return value
 */
void DOut_Safety_Trigger(uint8_t * u8_Channel)
{
    uint8_t u8_Safety;
    for(u8_Safety = 0; u8_Safety < 4; u8_Safety++)
    {
        switch((u8_Channel[u8_Safety] == 1) && ((s_DOutTable[u8_Safety + 6].u8_Mode >= SAFETY_MODE_LOW)))
        {
        case true:
            if(s_DOutTable[u8_Safety + 6].u8_SafetyVal == 0)
            {
                Safety_Mode_Change(u8_Safety + 6, 0);
            }
            else
            {
                Safety_Mode_Change(u8_Safety + 6, s_DOutTable[u8_Safety + 6].u8_DigitalOutput);
            }
            break;

        default:
            break;

        }
    }
}

/*
 * Function: DOut_Safety_Value_Set()
 *
 * @brief       Function to be called to change a pin's safety value
 * @param       u8_Pin ---> Pin to Change the Safety Value
 *              u8_Value ---> Tells if the output is either High or Low
 * @retval      No return value
 */
void DOut_Safety_Value_Set(uint8_t u8_Pin, uint8_t u8_Value)
{
    if((u8_Pin >= 6) && (u8_Pin <= 10))
    {
        s_DOutTable[u8_Pin].u8_SafetyVal = u8_Value;
    }
}

/*
 * Function: DOut_Safety_Mode_Set()
 *
 * @brief       Function to be called to change a pin to Safety Mode
 * @param       u8_Pin ---> Pin to be changed to Safety Mode
 *              u8_State ---> Tells if the output is either High or Low
 * @retval      No return value
 */
void DOut_Safety_Mode_Set(uint8_t u8_Pin, uint8_t u8_State)
{
    if((u8_Pin >= 6) && (u8_Pin <= 10))
    {
        if(u8_State == 0)
        {
            s_DOutTable[u8_Pin].u8_Mode = SAFETY_MODE_LOW;
            s_DOutTable[u8_Pin].b_Update = true;
        }
        else
        {
            s_DOutTable[u8_Pin].u8_Mode = SAFETY_MODE_HIGH;
            s_DOutTable[u8_Pin].b_Update = true;
        }
    }
}

/*
 * Function: DOut_Logic_Mode_Set()
 *
 * @brief       Function to be called to change a pin to Logic mode
 * @param       u8_Pin ---> Pin to be changed to Logic Mode
 *              u8_State ---> Tells if the output is either High or Low
 * @retval      No return value
 */
void DOut_Logic_Mode_Set(uint8_t u8_Pin, uint8_t u8_State)
{
    if(u8_State == 0)
    {
        s_DOutTable[u8_Pin].u8_Mode = LOGIC_MODE_LOW;
        s_DOutTable[u8_Pin].b_Update = true;

    }
    else
    {
        s_DOutTable[u8_Pin].u8_Mode = LOGIC_MODE_HIGH;
        s_DOutTable[u8_Pin].b_Update = true;

    }
}

/*
 * Function: DOut_DutyCycle_Set()
 *
 * @brief       Function to be called to change the duty cycle of a pin
 * @param       u8_Pin ---> Pin to be changed to PWM Mode
 * @retval      No return value
 */
void DOut_DutyCycle_Set(uint8_t u8_Pin, uint8_t u8_Duty)
{
    switch((u8_Duty <= 100) && (u8_Pin <= 3))
    {
    case true:
        s_DOutTable[u8_Pin].u8_DutyCycle = u8_Duty;
        s_DOutTable[u8_Pin].b_Update = true;
        break;

    default:
        break;
    }
}

/*
 * Function: DOut_PWM_Mode_Set()
 *
 * @brief       Function to be called to change the frequency of a pin
 * @param       u8_Pin ---> Pin to be changed to PWM Mode
 * @retval      No return value
 */
void DOut_Frequency_Set(uint8_t u8_Pin, uint16_t u16_Freq)
{
    switch(((u16_Freq <= 20000) && (u16_Freq >= 500)) && (u8_Pin <= 3))
    {
    case true:
        s_DOutTable[u8_Pin].u16_Value = u16_Freq;
        s_DOutTable[u8_Pin].b_Update = true;
        break;

    default:
        break;
    }
}
/*
 * Function: DOut_PWM_Mode_Set()
 *
 * @brief       Function to be called to change a pin to PWM mode
 * @param       u8_Pin ---> Pin to be changed to PWM Mode
 * @retval      No return value
 */
void DOut_PWM_Mode_Set(uint8_t u8_Pin)
{
    if(u8_Pin < 6)
    {
        s_DOutTable[u8_Pin].u8_Mode = PWM_MODE;
        s_DOutTable[u8_Pin].b_Update = true;

    }
}


/*
 * Function: DOut_Pin_Handler()
 *
 * @brief       Function to be called by systick to check each pin and change them
 * @param       No Parameter
 * @retval      No return value
 */
void DOut_Pin_Handler()
{
    uint8_t u8_PinsHandler;

    for(u8_PinsHandler = 0; u8_PinsHandler < 10; u8_PinsHandler++)
    {
        if(s_DOutTable[u8_PinsHandler].b_Update == true)
        {
            General_Pin_Change(u8_PinsHandler);
            s_DOutTable[u8_PinsHandler].b_Update = false;
        }
    }
}
/*
 * Function: General_Pin_Change()
 *
 * @brief       Function to be called to change the mode of a Pin
 * @param       u8_Pin ---> Pin that needs to be changed
 * @retval      No return value
 */

void General_Pin_Change(uint8_t u8_Pin)
{
    switch(s_DOutTable[u8_Pin].u8_Mode)
    {
    case LOGIC_MODE_LOW:
        if(u8_Pin < 4)
        {
            Disable_PWM(u8_Pin);
        }
        if(u8_Pin < 6)
        {
            Logic_Mode_Change(u8_Pin, s_DOutTable[u8_Pin].u8_DigitalOutput);
        }
        else
        {
            Logic_Mode_Change(u8_Pin, 0);
        }
        break;

    case LOGIC_MODE_HIGH:
        if(u8_Pin < 4)
        {
            Disable_PWM(u8_Pin);
        }
        if(u8_Pin < 6)
        {
            Logic_Mode_Change(u8_Pin, 0);
        }
        else
        {
            Logic_Mode_Change(u8_Pin, s_DOutTable[u8_Pin].u8_DigitalOutput);
        }
        break;

    case PWM_MODE:
        PWM_Mode_Change(u8_Pin, 16000000 / s_DOutTable[u8_Pin].u16_Value ,
                        (s_DOutTable[u8_Pin].u8_DutyCycle * (16000000 / s_DOutTable[u8_Pin].u16_Value) / 100));
        break;

    case SAFETY_MODE_LOW:
        Safety_Mode_Change(u8_Pin, 0);
        break;

    case SAFETY_MODE_HIGH:
        Safety_Mode_Change(u8_Pin, s_DOutTable[u8_Pin].u8_DigitalOutput);
        break;

    default:
        break;

    }
}

/*
 * Function: Digital_Outputs_Init()
 *
 * @brief       Initializes the digital output pins
 * @param       No parameters
 * @retval      No return value
 */

void Digital_Outputs_Init()
{
    Digital_Pin_Config();

    uint8_t u8_Pins;
    for(u8_Pins = 0; u8_Pins < 4; u8_Pins++)
    {
        Dout_CCP_Init(u8_Pins);
    }


}
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
