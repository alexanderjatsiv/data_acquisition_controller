/******************************************************************************/
/**
  * @file           das_d_inputs.c
  * @brief          Description of this file.
  */
/*  Dependencies: List all dependencies of this module. */

/*  Revision: */
/*  Jun 16, 2020        Rev/Sprint  Initials  Remarks */ 
/******************************************************************************/
/** @addtogroup _das_d_inputs
  * @{
  */ 

/** @defgroup _das_d_inputs_Source Source
  * @{
  */

/* Includes *******************************************************************/

#include "hdr/das_d_inputs.h"

/* Private Macros *************************************************************/
/** @defgroup _das_d_inputs_Private_Macros Private Macros
  * @{
  */

/**
  * @}
  */

/* Private Constants **********************************************************/
/** @defgroup _das_d_inputs_Private_Constants Private Constants
  * @{
  */

/**
  * @}
  */

/* Private Variables **********************************************************/
/** @defgroup _das_d_inputs_Private_Variables Private Variables
  * @{
  */
uint32_t u32_Timer0Timeus = 1000000;
uint32_t u32_Timer4Timeus = 1000000;

bool b_DInSafety = false;

s_DIn_Table s_DInTable[6] =
{
             {DIN0_PIN, 0, LOGIC_MODE, 0, 0, 0, SAFETY_NONE, {0,0,0,0}},
             {DIN1_PIN, 1, LOGIC_MODE, 0, 0, 0, SAFETY_NONE, {0,0,0,0}},
             {DIN2_PIN, 2, LOGIC_MODE, 0, 0, 0, SAFETY_NONE, {0,0,0,0}},
             {DIN3_PIN, 3, LOGIC_MODE, 0, 0, 0, SAFETY_NONE, {0,0,0,0}},
             {DIN4_PIN, 1, LOGIC_MODE, 0, 0, 0, SAFETY_NONE, {0,0,0,0}},
             {DIN5_PIN, 4, LOGIC_MODE, 0, 0, 0, SAFETY_NONE, {0,0,0,0}}
};

/**
  * @}
  */

/* Private functions **********************************************************/
/** @defgroup _das_d_inputs_Private_Functions Private Functions
  * @{
  */

/*
 * Function: Frequency_Read()
 *
 * @brief       Reads the frequency from the select pin
 * @param       u8_Pin    ---> The Pin to read from
 * @retval      No return value
 */
static void Frequency_Read(uint8_t u8_Pins)
{
    //Reads the frequency from the PORT M pins that are enabled to Frequency Mode
    switch(u8_Pins)
    {
        //Pin PM0
        case 0:
            // (Loaded value - Current Timer Value) * 1000000 / (Interrupt Timer period in us)
            s_DInTable[0].u16_Value = (((0xFFFF - MAP_TimerValueGet(TIMER2_BASE, TIMER_A))* 1000000) / u32_Timer4Timeus);

            //Reset the Timer Register value so no overflow conditions can occur unless at really high frequencies
            TIMER2A_VAL_REG = 0xFFFF;

            break;

        //Pin PM1
        case 1:
            s_DInTable[1].u16_Value = (((0xFFFF - MAP_TimerValueGet(TIMER2_BASE, TIMER_B))* 1000000) / u32_Timer4Timeus) ;
            TIMER2B_VAL_REG = 0xFFFF;
            break;

        //Pin PM2
        case 2:
            s_DInTable[2].u16_Value = (((0xFFFF - MAP_TimerValueGet(TIMER3_BASE, TIMER_A))* 1000000) / u32_Timer4Timeus) ;
            TIMER3A_VAL_REG = 0xFFFF;
            break;

        //Pin PM3
        case 3:
            s_DInTable[3].u16_Value = (((0xFFFF - MAP_TimerValueGet(TIMER3_BASE, TIMER_B))* 1000000) / u32_Timer4Timeus) ;
            TIMER3B_VAL_REG = 0xFFFF;
            break;

        default:
            break;
    }
}

/*
 * Function: Timer_Enable_CCP()
 *
 * @brief       Disable the timers that are configured to read frequency
 * @param       u8_Pin    ---> The Pin to read from
 * @retval      No return value
 */
static void Timer_Enable_CCP(uint8_t u8_Pins)
{
    //Enables the Timers on the PORT M
    switch(u8_Pins)
    {
        //Pin PM0
        case 0:
            MAP_TimerEnable(TIMER2_BASE, TIMER_A);
            break;

        //Pin PM1
        case 1:
            MAP_TimerEnable(TIMER2_BASE, TIMER_B);
            break;

        //Pin PM2
        case 2:
            MAP_TimerEnable(TIMER3_BASE, TIMER_A);
            break;

        //Pin PM3
        case 3:
            MAP_TimerEnable(TIMER3_BASE, TIMER_B);
            break;

        default:
            break;

    }
}
/*
 * Function: Timer_Disable_CCP()
 *
 * @brief       Disable the timers that are configured to read frequency
 * @param       u8_Pin    ---> The Pin to read from
 * @retval      No return value
 */
static void Timer_Disable_CCP(uint8_t u8_Pins)
{
    //Function to disable the Timers on the PORT M Pins.
    //Makes it more accurate to disable the timers first, read the values
    //and reset the register value, and then reenable those pins in the ISR.
    switch(u8_Pins)
    {
        //Pin PM0
        case 0:
            MAP_TimerDisable(TIMER2_BASE, TIMER_A);
            break;

        //Pin PM1
        case 1:
            MAP_TimerDisable(TIMER2_BASE, TIMER_B);
            break;

        //Pin PM2
        case 2:
            MAP_TimerDisable(TIMER3_BASE, TIMER_A);
            break;

        //Pin PM3
        case 3:
            MAP_TimerDisable(TIMER3_BASE, TIMER_B);
            break;

        default:
            break;

    }
}


/*
 * Function: Digital_Read()
 *
 * @brief       Reads the logical input value from a specific pin and stores it
 *              into the DIn table
 * @param       u8_Pin    ---> The Pin to read from
 * @retval      No return value
 */
static void Digital_Read(uint8_t u8_Pin)
{
    //Checks the PORT M pins.
   if(u8_Pin < 4)
   {
       //Since the 1 or 0 is on the nth bit based on the nth Pin Number, I have to left shift the reading
       //by that Pin Number.
       //This also reads the PORT M Pins
       s_DInTable[u8_Pin].u16_Value = (MAP_GPIOPinRead(DINM_PORT, s_DInTable[u8_Pin].u8_DigitalInput) >> s_DInTable[u8_Pin].u8_PinNo);
   }
   else
   {
       //This reads from PORT Q pins.
       s_DInTable[u8_Pin].u16_Value = (MAP_GPIOPinRead(DINQ_PORT, s_DInTable[u8_Pin].u8_DigitalInput) >> s_DInTable[u8_Pin].u8_PinNo);
   }
}

/*
 * Function: Frequency_Init()
 *
 * @brief       Initializes all of the PORT M pins timers to frequency mode
 * @param       u8_Pin --> The Pin that will have its timer initialized to frequency mode
 * @retval      No return value
 */
static void Frequency_Init(uint8_t u8_Pin)
{

    switch(u8_Pin)
    {
    //Pin PM0
    case 0:
        //Configure the Specific Timer for the device on the Pin
        MAP_GPIOPinConfigure(GPIO_PM0_T2CCP0);

        //Configure the Pin as a Timer
        MAP_GPIOPinTypeTimer(DINM_PORT, DIN0_PIN);

        //Initialize the timer specific to PM0
        General_Edge_Timer_Init(SYSCTL_PERIPH_TIMER2, TIMER2_BASE, TIMER_A);
        break;

    //Pin PM1
    case 1:
        MAP_GPIOPinConfigure(GPIO_PM1_T2CCP1);
        MAP_GPIOPinTypeTimer(DINM_PORT, DIN1_PIN);
        General_Edge_Timer_Init(SYSCTL_PERIPH_TIMER2, TIMER2_BASE, TIMER_B);
        break;

    //Pin PM2
    case 2:
        MAP_GPIOPinConfigure(GPIO_PM2_T3CCP0);
        MAP_GPIOPinTypeTimer(DINM_PORT, DIN2_PIN);
        General_Edge_Timer_Init(SYSCTL_PERIPH_TIMER3, TIMER3_BASE, TIMER_A);
        break;

    //Pin PM1
    case 3:
        MAP_GPIOPinConfigure(GPIO_PM3_T3CCP1);
        MAP_GPIOPinTypeTimer(DINM_PORT, DIN3_PIN);
        General_Edge_Timer_Init(SYSCTL_PERIPH_TIMER3, TIMER3_BASE, TIMER_B);
        break;

    //If none of the Pins are put through here just break out.
    default:
        break;

    }
}

/*
 * Function: General_Edge_Timer_Init()
 *
 * @brief       Initializes any timer with the edge count mode
 * @param       u32_Periph --> Timer peripheral
 *              u32_Base ---> Timer Base
 *              u32_Timer ---> Timer A or B
 * @retval      No return value
 */
static void General_Edge_Timer_Init(uint32_t u32_Periph, uint32_t u32_Base, uint32_t u32_Timer)
{
    //Enable timer peripheral
    MAP_SysCtlPeripheralEnable(u32_Periph);

    //Sets the timer to the system clock
    MAP_TimerClockSourceSet(u32_Base,
                                TIMER_CLOCK_SYSTEM);

    //Configures the timer to be split and to be in edge count down mode
    MAP_TimerConfigure(u32_Base,
                       (TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_CAP_COUNT| TIMER_CFG_B_CAP_COUNT));

    //Sets the event that will cause the timer to count down.
    //In this cause the timer will count whenever there is a low to high change.
    MAP_TimerControlEvent(u32_Base, u32_Timer, TIMER_EVENT_POS_EDGE);

    //Sets the load value. This value should be the maximum amount for the timer.
    MAP_TimerLoadSet(u32_Base,
                     u32_Timer,
                     0xFFFF);

    //Set the match value. This value should be at 0.
    MAP_TimerMatchSet(u32_Base,
                      u32_Timer,
                      0);

}
/**
  * @}
  */

/* Public functions ***********************************************************/
/** @defgroup _das_d_inputs_Public_Functions Public Functions
  * @{
  */



/*
 * Function: DInTest0()
 *
 * @brief       Changes all the PORT M pins to frequency mode
 * @param       No Parameters
 * @retval      No return value
 */
void DInTest0()
{
    Digital_Inputs_Init();
    Timer4_Init();

    uint8_t u8_DInTest;

    for(u8_DInTest = 0; u8_DInTest < 4; u8_DInTest++)
    {
        Frequency_Mode(u8_DInTest);
    }
}

/*
 * Function: DIn_Test1()
 *
 * @brief       Changes every other PORT M pin to frequency mode
 * @param       u8_Pin --> The Pin that will be changed to frequency mode
 * @retval      No return value
 */
void DInTest1()
{
    Digital_Inputs_Init();
    Timer4_Init();

    uint8_t u8_DInTest;

    for(u8_DInTest = 0; u8_DInTest < 4; u8_DInTest = u8_DInTest + 2)
    {
        Frequency_Mode(u8_DInTest);
    }
}

/*
 * Function: DInTest2()
 *
 * @brief       Checks to see Mode switching will work
 * @param       No Parameters
 * @retval      No return value
 */
void DInTest2()
{
    Digital_Inputs_Init();
    Timer4_Init();

    uint8_t u8_DInTest;

    for(u8_DInTest = 0; u8_DInTest < 4; u8_DInTest++)
    {
        Frequency_Mode(u8_DInTest);
    }
    Digital_Mode(0);

    Frequency_Mode(0);

}

/*
 * Function: DIn_Test3()
 *
 * @brief       Checks to see if the safety functions work
 * @param       No Parameters
 * @retval      No return value
 */
void DInTest3()
{
    uint8_t u8_Channel[4] = {0,0,0,0};
    DIn_Safety_Update(5, SAFETY_LOW, 0, 0, u8_Channel);
    Digital_Inputs_Init();
    Timer4_Init();

    uint8_t u8_DInTest;

    for(u8_DInTest = 0; u8_DInTest < 4; u8_DInTest++)
    {
        Frequency_Mode(u8_DInTest);
    }
    Digital_Mode(0);

    Frequency_Mode(0);

}

/*
 * Function: Digital_Readall()
 *
 * @brief   Checks all the DIn
 *
 * @param   No Param
 *
 * @retval  No return value
 */
void Digital_Readall()
{
    uint8_t u8_LogicPins;
    for(u8_LogicPins = 0; u8_LogicPins < 6; u8_LogicPins++)
    {
        if(s_DInTable[u8_LogicPins].u8_Mode == LOGIC_MODE)
        {
           Digital_Read(u8_LogicPins);
        }
    }

    for(u8_LogicPins = 0; u8_LogicPins < 6; u8_LogicPins++)
    {
        if(s_DInTable[u8_LogicPins].u8_Mode == LOGIC_MODE)
        {
           DIn_Safety_Check(u8_LogicPins, s_DInTable[u8_LogicPins].u16_Value);
        }
    }

}

/*
 * Function: DIn_Safety_Check()
 *
 * @brief   Checks a single DIn Pin
 *
 * @param   u8_pin ---> Pin to update the safety values
 *          u8_Mode ---> Mode to switch to
 *          u16_Low ---> 16 bit integer with the low value
 *          u16_High ---> 16 bit integer with the high value
 *          u8_channel ---> Array with the safety channels to be enabled
 *
 * @retval  No return value
 */

void DIn_Safety_Check(uint8_t u8_Pin, uint16_t u16_Value)
{
    switch(s_DInTable[u8_Pin].u8_SafetyMode)
    {
    case SAFETY_WINDOW:
        if((s_DInTable[u8_Pin].u16_SafetyLow >= u16_Value) ||
                (s_DInTable[u8_Pin].u16_SafetyHigh <= u16_Value))
        {
            if(b_DInSafety != true)
            {
                DOut_Safety_Trigger(s_DInTable[u8_Pin].u8_Channel);
            }
            b_DInSafety = true;
        }
        break;

    case SAFETY_LOW:
        if(s_DInTable[u8_Pin].u16_SafetyLow >= u16_Value)
        {
            if(b_DInSafety != true)
            {
                DOut_Safety_Trigger(s_DInTable[u8_Pin].u8_Channel);
            }
            b_DInSafety = true;
        }
        break;

    case SAFETY_HIGH:
        if(s_DInTable[u8_Pin].u16_SafetyHigh <= u16_Value)
        {
            if(b_DInSafety != true)
            {
                DOut_Safety_Trigger(s_DInTable[u8_Pin].u8_Channel);
            }
            b_DInSafety = true;
        }
        break;

    default:
        break;
    }
}

/*
 * Function: DIn_Safety_Update()
 *
 * @brief   Updates the safety values for the DIn Pin
 *
 * @param   u8_pin ---> Pin to update the safety values
 *          u8_Mode ---> Mode to switch to
 *          u16_Low ---> 16 bit integer with the low value
 *          u16_High ---> 16 bit integer with the high value
 *          u8_channel ---> Array with the safety channels to be enabled
 *
 * @retval  No return value
 */
void DIn_Safety_Update(uint8_t u8_Pin, uint8_t u8_Mode, uint16_t u16_Low, uint16_t u16_High, uint8_t * u8_channel)
{

    uint8_t u8_CH = 0;
    switch(u8_Mode){
    case SAFETY_WINDOW:
        for(u8_CH = 0; u8_CH < 4; u8_CH++)
        {
            s_DInTable[u8_Pin].u8_Channel[u8_CH] = u8_channel[u8_CH];
        }
        s_DInTable[u8_Pin].u16_SafetyHigh = u16_High;
        s_DInTable[u8_Pin].u16_SafetyLow = u16_Low;
        s_DInTable[u8_Pin].u8_SafetyMode = SAFETY_WINDOW;
        break;

    case SAFETY_LOW:
        for(u8_CH = 0; u8_CH < 4; u8_CH++)
        {
            s_DInTable[u8_Pin].u8_Channel[u8_CH] = u8_channel[u8_CH];
        }
        s_DInTable[u8_Pin].u16_SafetyLow = u16_Low;
        s_DInTable[u8_Pin].u8_SafetyMode = SAFETY_LOW;
        break;

    case SAFETY_HIGH:
        for(u8_CH = 0; u8_CH < 4; u8_CH++)
        {
            s_DInTable[u8_Pin].u8_Channel[u8_CH] = u8_channel[u8_CH];
        }
        s_DInTable[u8_Pin].u16_SafetyHigh = u16_High;
        s_DInTable[u8_Pin].u8_SafetyMode = SAFETY_HIGH;
        break;

    case SAFETY_NONE:
        s_DInTable[u8_Pin].u8_SafetyMode = SAFETY_NONE;
        break;

    default:
        break;
    }
}


/*
 * Function: Digital_Mode()
 *
 * @brief       Changes any of the pins to digital mode
 * @param       u8_Pin --> The Pin that will be changed to digital mode
 * @retval      No return value
 */

void Digital_Mode(uint8_t u8_Pin)
{
    switch(s_DInTable[u8_Pin].u8_Mode)
    {
        case CCP_MODE:
            switch(u8_Pin)
            {
            case 0:
                s_DInTable[u8_Pin].u8_Mode = LOGIC_MODE;
                MAP_TimerDisable(TIMER2_BASE, TIMER_A);
                break;

            case 1:
                s_DInTable[u8_Pin].u8_Mode = LOGIC_MODE;
                MAP_TimerDisable(TIMER2_BASE, TIMER_B);
                break;
            case 2:
                s_DInTable[u8_Pin].u8_Mode = LOGIC_MODE;
                MAP_TimerDisable(TIMER3_BASE, TIMER_A);
                break;
            case 3:
                s_DInTable[u8_Pin].u8_Mode = LOGIC_MODE;
                MAP_TimerDisable(TIMER3_BASE, TIMER_B);
                break;

            default:
                break;
            }

        default:
             break;
    }
}

/*
 * Function: Frequency_Mode()
 *
 * @brief       Changes a pin to frequency mode
 * @param       u8_Pin --> The Pin that will be changed to frequency mode
 * @retval      No return value
 */
void Frequency_Mode(uint8_t u8_Pin)
{
    switch(u8_Pin)
    {
    case 0:
        s_DInTable[u8_Pin].u8_Mode = CCP_MODE;
        MAP_TimerEnable(TIMER2_BASE, TIMER_A);
        break;

    case 1:
        s_DInTable[u8_Pin].u8_Mode = CCP_MODE;
        MAP_TimerEnable(TIMER2_BASE, TIMER_B);
        break;

    case 2:
        s_DInTable[u8_Pin].u8_Mode = CCP_MODE;
        MAP_TimerEnable(TIMER3_BASE, TIMER_A);
        break;

    case 3:
        s_DInTable[u8_Pin].u8_Mode = CCP_MODE;
        MAP_TimerEnable(TIMER3_BASE, TIMER_B);
        break;

    default:
        break;
    }

}



/*
 * Function: Timer4_Init()
 *
 * @brief       Initializes timer4 with 1ms interrupts to record frequency
 * @param       No parameters
 * @retval      No return value
 */
void Timer4_Init()
{
    //Enable timer peripheral
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER4);

    //Configures the system clock as the clock source
    MAP_TimerClockSourceSet(TIMER4_BASE,            //Timer0 Clock
                            TIMER_CLOCK_SYSTEM);    //Set to clock system instead of precision internal osc

    //Configures the timer in periodic mode
    //No need to reset the TWEN bit in periodic mode
    MAP_TimerConfigure(TIMER4_BASE,
                       TIMER_CFG_PERIODIC);         //Periodic Timer and counts down

    //Loads the starting value for the timer
    MAP_TimerLoadSet(TIMER4_BASE,
                     TIMER_A,
                     120*u32_Timer4Timeus);

    //Puts the lower bound for the timer
    MAP_TimerMatchSet(TIMER4_BASE,
                      TIMER_A,
                      0);



    //Prevent timer from counting when halted by debugger
    MAP_TimerControlStall(TIMER4_BASE,
                          TIMER_A,
                          true);        //Stops the timer while in debug mode. Will help with debugging so that it does not
                                        //interrupt every time the code is ran.

    //Enable what causes the interrupt for the timer
    MAP_TimerIntEnable(TIMER4_BASE, TIMER_TIMA_TIMEOUT);

    //Enable the interrupt
    MAP_IntEnable(INT_TIMER4A);

    //Enable the Timer
    MAP_TimerEnable(TIMER4_BASE, TIMER_A);
}

/*
 * Function: Timer0_Init()
 *
 * @brief       Initializes timer0 on a 1ms frequency
 * @param       No parameters
 * @retval      No return value
 */
void Timer0_Init()
{
    //Enable timer peripheral
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);

    //Configures the system clock as the clock source
    MAP_TimerClockSourceSet(TIMER0_BASE,            //Timer0 Clock
                            TIMER_CLOCK_SYSTEM);    //Set to clock system instead of precision internal osc

    //Configures the Timer to be on Periodic and count down mode
    MAP_TimerConfigure(TIMER0_BASE,
                       TIMER_CFG_PERIODIC);

    //Load timer for 1ms frequency
    MAP_TimerLoadSet(TIMER0_BASE,
                     TIMER_A,
                     120*u32_Timer0Timeus);

    //Set the match value at 0. When the load value equals the
    //match value then an interrupt will occur
    MAP_TimerMatchSet(TIMER0_BASE,
                      TIMER_A,
                      0);



    //Prevent timer from counting when halted by debugger
    MAP_TimerControlStall(TIMER0_BASE,
                          TIMER_A,
                          true);


    //Enable the interrupt with what will cause the interrupt
    //which is when the timer equals the match value
    MAP_TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    //Enable the interrupt in the system
    MAP_IntEnable(INT_TIMER0A);

    //Enable the Timer in the system
    MAP_TimerEnable(TIMER0_BASE, TIMER_A);
}

/*
 * Function: Digital_Inputs_Init()
 *
 * @brief       Initializes the digital input pins
 * @param       No parameters
 * @retval      No return value
 */

void Digital_Inputs_Init(void)
{
    //Enable DIN 0-3
    MAP_SysCtlPeripheralEnable(DINM_PERIPH);
    MAP_GPIOPinTypeGPIOInput(DINM_PORT, DIN0_PIN | DIN1_PIN | DIN2_PIN | DIN3_PIN);

    //Enable DIN 4-5
    MAP_SysCtlPeripheralEnable(DINQ_PERIPH);
    MAP_GPIOPinTypeGPIOInput(DINQ_PORT, DIN4_PIN | DIN5_PIN);

    uint8_t u8_CCPPins;
    for(u8_CCPPins = 0; u8_CCPPins < 4; u8_CCPPins++)
    {
        Frequency_Init(u8_CCPPins);
    }
}

/*
 * Function: Timer0IntHandler()
 *
 * @brief       The interrupt handler for timer 0. Used to read frequency
 * @param       No parameters
 * @retval      No return value
 */
void Timer0IntHandler(void)
{
    //Clear the interrupt
    MAP_TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    Digital_Readall();


}

/*
 * Function: Timer4IntHandler()
 *
 * @brief       The interrupt handler for timer 4.
 * @param       No parameters
 * @retval      No return value
 */
void Timer4IntHandler(void)
{
    //Clear the interrupt
    MAP_TimerIntClear(TIMER4_BASE, TIMER_TIMA_TIMEOUT);

    uint8_t u8_CCPPins;

    //Disable all the CCP timers so none of them are running while
    //we are checking their values
    for(u8_CCPPins = 0; u8_CCPPins < 4; u8_CCPPins++)
    {
        if(s_DInTable[u8_CCPPins].u8_Mode == CCP_MODE)
        {
           Timer_Disable_CCP(u8_CCPPins);
        }
    }

    //Read the values from the timer register and calculate the frequency
    //This will also reset the the timer register back to its max value
    for(u8_CCPPins = 0; u8_CCPPins < 4; u8_CCPPins++)
        {
            if(s_DInTable[u8_CCPPins].u8_Mode == CCP_MODE)
            {
               Frequency_Read(u8_CCPPins);
               DIn_Safety_Check(u8_CCPPins, s_DInTable[u8_CCPPins].u16_Value);
            }
        }

    //Enable the timers to be back on its frequency mode
    for(u8_CCPPins = 0; u8_CCPPins < 4; u8_CCPPins++)
        {
            if(s_DInTable[u8_CCPPins].u8_Mode == CCP_MODE)
            {
               Timer_Enable_CCP(u8_CCPPins);
            }
        }
}

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
