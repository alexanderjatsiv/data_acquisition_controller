/******************************************************************************/
/**
  * @file           das_a_inputs.c
  * @brief          Description of this file.
  */
/*  Dependencies: List all dependencies of this module. */

/*  Revision: */
/*  May 22, 2020        Rev/Sprint  Initials  Remarks */ 
/******************************************************************************/
/** @addtogroup _das_a_inputs
  * @{
  */ 

/** @defgroup _das_a_inputs_Source Source
  * @{
  */

/* Includes *******************************************************************/

//Including the Header File
#include "hdr/das_a_inputs.h"




/* Private Macros *************************************************************/
/** @defgroup _das_a_inputs_Private_Macros Private Macros
  * @{
  */

/**
  * @}
  */

/* Private Constants **********************************************************/
/** @defgroup _das_a_inputs_Private_Constants Private Constants
  * @{
  */

/**
  * @}
  */

/* Private Variables **********************************************************/
/** @defgroup _das_a_inputs_Private_Variables Private Variables
  * @{
  */

/*
 * ADC TEST Variables
 */
uint8_t u8_ADCtest;
uint16_t u16_changeFreqency;


/**
 * Counts active AIN channels
 */
uint8_t activeInputs;                           //Counts active AIN channels


/**
 * ADC Buffer
 */
uint32_t u32_AInBuffer[TOTAL_ANALOG_INPUTS];         //Holds data stored from analog inputs




/**
 * ADC Table
 */

s_ADCTableStruct s_ADCTable =
{
         {
                              64,
                              5000
         },
         {

                     /*HV0*/           {AIHV0, false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*HV1*/           {AIHV1, false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*HV2*/           {AIHV2, false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*HV3*/           {AIHV3, false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*HV4*/           {AIHV4, false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*HV5*/           {AIHV5, false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*HV6*/           {AIHV6, false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*HV7*/           {AIHV7, false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*HV8*/           {AIHV8, false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*HV9*/           {AIHV9, false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*HV10*/          {AIHV10,false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*HV11*/          {AIHV11,false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*LV0*/           {AILV0, false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*LV1*/           {AILV1, false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*LV2*/           {AILV2, false, SAFETY_NONE, 0, 0, {0,0,0,0}},
                     /*LV3*/           {AILV3, false, SAFETY_NONE, 0, 0, {0,0,0,0}}

         }

};

//Boolean for the safety control

bool b_SafetyControl = false;


/**
  * @}
  */

/* Private functions **********************************************************/
/** @defgroup _das_a_inputs_Private_Functions Private Functions
  * @{
  */

/*  Function:   functionName */
/**
  *  @brief     Function description
  *  @param     Function parameters
  *  @retval    Return value
  */




/**
  * @}
  */

/* Public functions ***********************************************************/
/** @defgroup _das_a_inputs_Public_Functions Public Functions
  * @{
  */

/*  Function:   functionName */
/**
  *  @brief     Function description
  *  @param     Function parameters
  *  @retval    Return value
  */

/**
 * Function: ADC_TEST0()
 *
 * @brief   This function tests the code to make sure the ADC
 *          reads from all 16 inputs
 * @param   No parameter
 * @retval  No return value
 */
void ADC_TEST0()
{
    uint8_t u8_test;
    for(u8_test = 0; u8_test < 16; u8_test++)
    {
        s_ADCTable.s_AnalogInputsTable[u8_test].b_active = true;
    }
    ADC_initializer();
}

/**
 * Function: ADC_TEST1()
 *
 * @brief   This function tests the code to make sure the ADC
 *          reads from 1 of the pins
 * @param   No parameter
 * @retval  No return value
 */
void ADC_TEST1()
{
    uint8_t u8_test;
    for(u8_test = 0; u8_test < 8; u8_test++)
    {
        s_ADCTable.s_AnalogInputsTable[u8_test].b_active = true;
    }
    ADC_initializer();
}

/**
 * Function: ADC_TEST2()
 *
 * @brief   This function tests the code to make sure the ADC
 *          reads every 4th input starting at input 0.
 * @param
 * @retval
 */
void ADC_TEST2()
{
    uint8_t u8_test;
    for(u8_test = 0; u8_test < 1; u8_test += 4)
        {
            s_ADCTable.s_AnalogInputsTable[u8_test].b_active = true;
        }
    ADC_initializer();
}


/**
 * Function: ADC_TEST3()
 *
 * @brief   This function tests the code to make sure the ADC
 *          reads every 4th input starting at input 0.
 * @param
 * @retval
 */
void ADC_TEST3()
{
    uint8_t u8_test;
    if(u8_ADCtest == 0)
    {
        for(u8_test = 0; u8_test < 16; u8_test++)
        {
            s_ADCTable.s_AnalogInputsTable[u8_test].b_active = true;
        }
        ADC_initializer();
        u8_ADCtest = 1;
    }
    else if((u8_ADCtest == 1) && (u16_changeFreqency >= 250))
    {
        ADC_Timer1_Frequency(520, 64);
        u8_ADCtest = 2;
    }
    else if((u8_ADCtest == 2) && (u16_changeFreqency >= 500))
    {
        ADC_Timer1_Frequency(1000000, 64);
        u8_ADCtest = 3;
    }
}



/*
 * Function: ADC_Safety_Check
 *
 * @brief   Triggers the safety digital output if the value of the
 *          pin reaches outside or equals its boundaries.
 *
 * @param   u8_pin ---> The pin that is being checked.
 *          u32_value ---> The value that is being checked.
 *
 * @retval  No return value needed.
 */



void ADC_Safety_Check(uint8_t u8_Pin, uint32_t u32_Value)
{
    switch(s_ADCTable.s_AnalogInputsTable[u8_Pin].u8_SafetyType)
    {
    case SAFETY_WINDOW:
        if((s_ADCTable.s_AnalogInputsTable[u8_Pin].u16_SafetyLow >= u32_Value) ||
                    (s_ADCTable.s_AnalogInputsTable[u8_Pin].u16_SafetyHigh <= u32_Value))
        {
            //This will probably change to make a software interrupt.
            if(b_SafetyControl == true)
            {
                DOut_Safety_Trigger(s_ADCTable.s_AnalogInputsTable[u8_Pin].u8_SafetyChannel);
            }
            b_SafetyControl = true;
        }
        break;

    case SAFETY_LOW:
        if(s_ADCTable.s_AnalogInputsTable[u8_Pin].u16_SafetyLow >= u32_Value)
        {
            if(b_SafetyControl == true)
            {
                DOut_Safety_Trigger(s_ADCTable.s_AnalogInputsTable[u8_Pin].u8_SafetyChannel);
            }
            b_SafetyControl = true;
        }
        break;

    case SAFETY_HIGH:
        if(s_ADCTable.s_AnalogInputsTable[u8_Pin].u16_SafetyHigh <= u32_Value)
        {
            if(b_SafetyControl == true)
            {
                DOut_Safety_Trigger(s_ADCTable.s_AnalogInputsTable[u8_Pin].u8_SafetyChannel);
            }
            b_SafetyControl = true;
        }
        break;

    default:
        break;

    }

}

/*
 * Function: ADC_Safety_Update()
 *
 * @brief   Updates the safety values for the ADC Pin
 *
 * @param   u8_pin ---> Pin to update the safety values
 *          u8_type ---> Array with the safety type
 *          u16_Low ---> 16 bit integer with the low value
 *          u16_High ---> 16 bit integer with the high value
 *          u8_channel ---> Array with the safety channels to be enabled
 *
 * @retval  No return value
 */
void ADC_Safety_Update(uint8_t u8_pin, uint8_t u8_Type, uint16_t u16_Low, uint16_t u16_High, uint8_t * u8_channel)
{
    uint8_t u8_CH = 0;

    switch(u8_Type)
    {
    case SAFETY_WINDOW:
        s_ADCTable.s_AnalogInputsTable[u8_pin].u8_SafetyType = SAFETY_WINDOW;
        s_ADCTable.s_AnalogInputsTable[u8_pin].u16_SafetyHigh = u16_High;
        s_ADCTable.s_AnalogInputsTable[u8_pin].u16_SafetyLow = u16_Low;
        for(u8_CH = 0; u8_CH < 4; u8_CH++)
        {
            s_ADCTable.s_AnalogInputsTable[u8_pin].u8_SafetyChannel[u8_CH] = u8_channel[u8_CH];
        }
        break;

    case SAFETY_HIGH:
        s_ADCTable.s_AnalogInputsTable[u8_pin].u8_SafetyType = SAFETY_HIGH;
        s_ADCTable.s_AnalogInputsTable[u8_pin].u16_SafetyHigh = u16_High;
        for(u8_CH = 0; u8_CH < 4; u8_CH++)
        {
            s_ADCTable.s_AnalogInputsTable[u8_pin].u8_SafetyChannel[u8_CH] = u8_channel[u8_CH];
        }
        break;

    case SAFETY_LOW:
        s_ADCTable.s_AnalogInputsTable[u8_pin].u8_SafetyType = SAFETY_LOW;
        s_ADCTable.s_AnalogInputsTable[u8_pin].u16_SafetyLow = u16_Low;
        for(u8_CH = 0; u8_CH < 4; u8_CH++)
        {
            s_ADCTable.s_AnalogInputsTable[u8_pin].u8_SafetyChannel[u8_CH] = u8_channel[u8_CH];
        }
        break;

    case SAFETY_NONE:
        s_ADCTable.s_AnalogInputsTable[u8_pin].u8_SafetyType = SAFETY_NONE;
        break;

    default:
        break;
    }
}

/*
 * Function: ADC_Mosfet_configure()
 *
 * @brief   Enables or disables the Mosfets for the specific ADC pin.
 *
 * @param   u8_ADCNo ---> The ADC port to enable the MOSFET
 *                        (8 bits only looking at values 0-15).
 *          b_config ---> A boolean to either enable or disable the FET.
 *
 * @retval  No return value needed.
 */

void ADC_Mosfet_configure(uint8_t u8_ADCNo, bool b_config)
{

    static s_analogGates_t s_mosfets[16] =
        {
                                                {AIHV0_GATE_PERIPH, AIHV0_GATE_PORT, AIHV0_GATE_PIN},
                                                {AIHV1_GATE_PERIPH, AIHV1_GATE_PORT, AIHV1_GATE_PIN},
                                                {AIHV2_GATE_PERIPH, AIHV2_GATE_PORT, AIHV2_GATE_PIN},
                                                {AIHV3_GATE_PERIPH, AIHV3_GATE_PORT, AIHV3_GATE_PIN},
                                                {AIHV4_GATE_PERIPH, AIHV4_GATE_PORT, AIHV4_GATE_PIN},
                                                {AIHV5_GATE_PERIPH, AIHV5_GATE_PORT, AIHV5_GATE_PIN},
                                                {AIHV6_GATE_PERIPH, AIHV6_GATE_PORT, AIHV6_GATE_PIN},
                                                {AIHV7_GATE_PERIPH, AIHV7_GATE_PORT, AIHV7_GATE_PIN},
                                                {AIHV8_GATE_PERIPH, AIHV8_GATE_PORT, AIHV8_GATE_PIN},
                                                {AIHV9_GATE_PERIPH, AIHV9_GATE_PORT, AIHV9_GATE_PIN},
                                                {AIHV10_GATE_PERIPH, AIHV10_GATE_PORT, AIHV10_GATE_PIN},
                                                {AIHV11_GATE_PERIPH, AIHV11_GATE_PORT, AIHV11_GATE_PIN},
                                                {AILV0_GATE_PERIPH, AILV0_GATE_PORT, AILV0_GATE_PIN},
                                                {AILV1_GATE_PERIPH, AILV1_GATE_PORT, AILV1_GATE_PIN},
                                                {AILV2_GATE_PERIPH, AILV2_GATE_PORT, AILV2_GATE_PIN},
                                                {AILV3_GATE_PERIPH, AILV3_GATE_PORT, AILV3_GATE_PIN}
        };

    if(b_config == true)
    {
        //Enable analog input
        MAP_GPIOPinWrite(s_mosfets[u8_ADCNo].u32_port, s_mosfets[u8_ADCNo].u32_pin, 0);
        //Store struct in global struct
        s_ADCTable.s_AnalogInputsTable[u8_ADCNo].b_active = b_config;
        activeInputs++;
    }
    else
    {
        //disable analog input
        MAP_GPIOPinWrite(s_mosfets[u8_ADCNo].u32_port, s_mosfets[u8_ADCNo].u32_pin, s_mosfets[u8_ADCNo].u32_pin);
        s_ADCTable.s_AnalogInputsTable[u8_ADCNo].b_active = b_config;
        activeInputs--;
    }

}
/**
 * Function:    ADC_Timer1_Frequency()
 *
 *@brief    Changes the frequency that Timer1 reads from the ADC.
 *@brief
 *@param    u32_Time_us ----> 32 bit integer of the Sampling Time in us.
 *@param    u8_SamplingRate ---> 8 bit integer of the Sampling Rate.
 *
 *@retval   Returns a -1 if it cannot change the frequency/sampling rate.
 *          Returns a 0 if it does change the frequency/sampling rate.
 *
 */
int ADC_Timer1_Frequency(uint32_t u32_Time_us, uint8_t u8_SamplingRate)
{
    //Disable ADC timer so that changes can be made
    MAP_TimerDisable(TIMER1_BASE,
                     TIMER_A);

    //Initialize the return value
    int8_t retval = 0;

    /* If the sampling time is less the the oversampling rate
     * multiplied by the amount of pins divided by 2 (the right shift operator)
     * which is the data transfer time, then do not change the frequency and return
     * the error code back.
     */
    if(u32_Time_us < ((16*u8_SamplingRate) >> 1))
    {
        retval = -1;
    }

    if(retval == 0)
    {
        //If the oversampling rate is not the same change it
        if(u8_SamplingRate != s_ADCTable.s_SamplingTable.u32_OversamplingRate)
        {
            MAP_ADCHardwareOversampleConfigure(ADC0_BASE, s_ADCTable.s_SamplingTable.u32_OversamplingRate);
            s_ADCTable.s_SamplingTable.u32_OversamplingRate = u8_SamplingRate;
        }

        //If the sampling time is not the same change it
        if(u32_Time_us != s_ADCTable.s_SamplingTable.u32_Timer1us)
        {
            MAP_TimerLoadSet(TIMER1_BASE,
                             TIMER_A,
                             120*u32_Time_us);
            s_ADCTable.s_SamplingTable.u32_Timer1us = u32_Time_us;
        }

    }

    //Enable the Timer
    MAP_TimerEnable(TIMER1_BASE, TIMER_A);

    //Return either a 0 or -1
    return retval;

}

/**
 *
 *Function  analog_mosfets_init
 *
 *@brief    Initializes the Mosfets for the ADC reading
 *@param    s_mosfets  ---> A structure with all of the ports and peripherals for the MOSFETS
 *          s_AnalogInputs ---> A structure of all of the Analog Inputs that are enabled
 *@retval   No return value
 */

static void analog_mosfets_init(s_analogInputs_t * s_AnalogInputs)
{
    //CONFIGURE MOSFETS FOR ANALOG INPUT

    static s_analogGates_t s_mosfets[16] =
        {
                                                {AIHV0_GATE_PERIPH, AIHV0_GATE_PORT, AIHV0_GATE_PIN},
                                                {AIHV1_GATE_PERIPH, AIHV1_GATE_PORT, AIHV1_GATE_PIN},
                                                {AIHV2_GATE_PERIPH, AIHV2_GATE_PORT, AIHV2_GATE_PIN},
                                                {AIHV3_GATE_PERIPH, AIHV3_GATE_PORT, AIHV3_GATE_PIN},
                                                {AIHV4_GATE_PERIPH, AIHV4_GATE_PORT, AIHV4_GATE_PIN},
                                                {AIHV5_GATE_PERIPH, AIHV5_GATE_PORT, AIHV5_GATE_PIN},
                                                {AIHV6_GATE_PERIPH, AIHV6_GATE_PORT, AIHV6_GATE_PIN},
                                                {AIHV7_GATE_PERIPH, AIHV7_GATE_PORT, AIHV7_GATE_PIN},
                                                {AIHV8_GATE_PERIPH, AIHV8_GATE_PORT, AIHV8_GATE_PIN},
                                                {AIHV9_GATE_PERIPH, AIHV9_GATE_PORT, AIHV9_GATE_PIN},
                                                {AIHV10_GATE_PERIPH, AIHV10_GATE_PORT, AIHV10_GATE_PIN},
                                                {AIHV11_GATE_PERIPH, AIHV11_GATE_PORT, AIHV11_GATE_PIN},
                                                {AILV0_GATE_PERIPH, AILV0_GATE_PORT, AILV0_GATE_PIN},
                                                {AILV1_GATE_PERIPH, AILV1_GATE_PORT, AILV1_GATE_PIN},
                                                {AILV2_GATE_PERIPH, AILV2_GATE_PORT, AILV2_GATE_PIN},
                                                {AILV3_GATE_PERIPH, AILV3_GATE_PORT, AILV3_GATE_PIN}
        };
        uint8_t u8_inputs;
        for (u8_inputs = 0; u8_inputs < TOTAL_ANALOG_INPUTS; u8_inputs++)
        {
            //Enable peripherals
            MAP_SysCtlPeripheralEnable(s_mosfets[u8_inputs].u32_periph);
            //Set pins as output
            MAP_GPIOPinTypeGPIOOutput(s_mosfets[u8_inputs].u32_port,
                                      s_mosfets[u8_inputs].u32_pin);
            //Configure pin drive settings
            MAP_GPIOPadConfigSet(s_mosfets[u8_inputs].u32_port,
                                 s_mosfets[u8_inputs].u32_pin,
                                 GPIO_STRENGTH_4MA,
                                 GPIO_PIN_TYPE_STD);

            //Check if analog input should be enabled
            if (s_AnalogInputs[u8_inputs].b_active == true)
            {
                //Enable analog input
                MAP_GPIOPinWrite(s_mosfets[u8_inputs].u32_port,
                                 s_mosfets[u8_inputs].u32_pin, 0);

                activeInputs++;
            }
            else
            {
                //disable analog input
                MAP_GPIOPinWrite(s_mosfets[u8_inputs].u32_port,
                                 s_mosfets[u8_inputs].u32_pin,
                                 s_mosfets[u8_inputs].u32_pin);
            }
        }
}
/**
 * ADC_Timer1_Initializer()
 *
 * @brief   This initializes the Timer1 for the ADC
 * @param   No Parameters
 * @retval  No return Value
 */
static void ADC_Timer1_Initializer(void)
{
    //Enable timer peripheral
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);

    //Configures the system clock as the clock source
    MAP_TimerClockSourceSet(TIMER1_BASE,            //Timer1 Clock
                            TIMER_CLOCK_SYSTEM);    //Set to clock system instead of precision internal osc

    MAP_TimerConfigure(TIMER1_BASE,
                       TIMER_CFG_PERIODIC);         //Periodic Timer and counts down

    //Enables the ADC trigger output
    MAP_TimerControlTrigger(TIMER1_BASE,
                            TIMER_A,
                            true);          //Enables the ADC's output trigger


    //Prevent timer from counting when halted by debugger
    MAP_TimerControlStall(TIMER1_BASE,
                          TIMER_A,
                          true);        //Stops the timer while in debug mode. Will help with debugging so that it does not
                                        //interrupt every time the code is ran.

    //Load timer for 1ms frequency
    MAP_TimerLoadSet(TIMER1_BASE,
                     TIMER_A,
                     120000);

}

/**
 *  Function:   uDMA_ADC_initializer()
 *
 *  @brief
 *  @param
 *  @retval
 */

static void uDMA_ADC_initializer()
{

    //Enables DMA for sequences 0, 1 and 2
    MAP_ADCSequenceDMAEnable(ADC0_BASE, 0);
    MAP_ADCSequenceDMAEnable(ADC0_BASE, 1);
    MAP_ADCSequenceDMAEnable(ADC0_BASE, 2);

    //Sequence 0 uDMA configuration
    //Enables attribute of uDMA channel to use Burst Mode
    MAP_uDMAChannelAttributeEnable(UDMA_CHANNEL_ADC0, UDMA_ATTR_USEBURST);

    MAP_uDMAChannelControlSet(UDMA_CHANNEL_ADC0|
                              UDMA_PRI_SELECT,  //Select whether Alternate or Primary Data Structure
                              UDMA_SIZE_32|     //Select the data size
                              UDMA_SRC_INC_NONE|    //Select the source address increment
                              UDMA_DST_INC_32|      //Select the destination address increment
                              UDMA_ARB_2);       //Select after how many items are passed that the DMA will try to acquire the BUS again

   //Set the transfer controls
    MAP_uDMAChannelTransferSet(UDMA_CHANNEL_ADC0|
                              UDMA_PRI_SELECT,  //Select Alternate or Primary Data Structure
                              UDMA_MODE_BASIC,  //Select uDMA mode of transferring data
                              (void *)(ADC0_BASE + ADC_O_SSFIFO0),  //Source Address
                              &u32_AInBuffer,    //Data buffer
                              8);               //Amount of Data Items to be read


   // Do this again for Channels 1 and 2

   //Sets control parameters for UDMA
   MAP_uDMAChannelControlSet(UDMA_CHANNEL_ADC1 |                   //ADC sequence 1
                             UDMA_PRI_SELECT,                      //Uses primary data structure
                             UDMA_SIZE_32 |                        //32bit data size
                             UDMA_SRC_INC_NONE |                   //No source address increment
                             UDMA_DST_INC_32 |                     //Destination address increment set to 32bits
                             UDMA_ARB_2);                          //Sends 2 items before bus is re-arbitrated
   //Sets transfer parameters for UDMA
   MAP_uDMAChannelTransferSet(UDMA_CHANNEL_ADC1 |                  //ADC sequence 1
                              UDMA_PRI_SELECT,                     //Uses primary data structure
                              UDMA_MODE_BASIC,                     //Performs a basic data transfer
                              (void *)(ADC0_BASE + ADC_O_SSFIFO1), //Source of data transfer
                              &u32_AInBuffer[8],             //Destination of data transfer (start at index 8)
                              4);                                  //Number of data items

   //Sequence 2 uDMA configuration
   //Enables attribute of UDMA channel
   MAP_uDMAChannelAttributeEnable(UDMA_CHANNEL_ADC2,               //ADC sequence 2
                                  UDMA_ATTR_USEBURST);             //Restrict transfers to burst only mode
   //Sets control parameters for UDMA
   MAP_uDMAChannelControlSet(UDMA_CHANNEL_ADC2 |                   //ADC sequence 2
                             UDMA_PRI_SELECT,                      //Uses primary data structure
                             UDMA_SIZE_32 |                        //32bit data size
                             UDMA_SRC_INC_NONE |                   //No source address increment
                             UDMA_DST_INC_32 |                     //Destination address increment set to 32bits
                             UDMA_ARB_4);                          //Sends 4 items before bus is re-arbitrated
   //Sets transfer parameters for UDMA
   MAP_uDMAChannelTransferSet(UDMA_CHANNEL_ADC2 |                  //ADC sequence 2
                              UDMA_PRI_SELECT,                     //Uses primary data structure
                              UDMA_MODE_BASIC,                     //Performs a basic data transfer
                              (void *)(ADC0_BASE + ADC_O_SSFIFO2), //Source of data transfer
                              &u32_AInBuffer[12],            //Destination of data transfer (start at index 12)
                              4);                                  //Number of data items
   //Enable UDMA channel
   MAP_uDMAChannelEnable(UDMA_CHANNEL_ADC0);

   //Enable UDMA channel
   MAP_uDMAChannelEnable(UDMA_CHANNEL_ADC1);

   //Enable UDMA channel
   MAP_uDMAChannelEnable(UDMA_CHANNEL_ADC2);

}

/**
  * Function: ADC_sequence_step_initializer()
  *
  * @brief      Creates the sequence on how the ADC will read the data
  * @param      int i_sequencer ---> An integer for the sequence number
  *             int i_priority ----> Priority of the sequence
  *             int i_length -----> Length of the sequence
  *             int s_sequence ----> The sequence order in a struct
  * @retval     No return value
 */

static void ADC_sequence_step_initializer(uint8_t u8_sequencerNo, uint8_t u8_priority, uint8_t u8_length, s_ADCsequence * s_sequence)
{

    uint8_t u8_order;
    MAP_ADCSequenceConfigure(ADC0_BASE,                     //ADC Base
                                 u8_sequencerNo,               //Sequencer 0 captures up to 8 samples
                                 ADC_TRIGGER_TIMER,         //ADC trigger is generated by timer
                                 u8_priority);               //Priority of sequencer 0

    for(u8_order = 0; u8_order < u8_length; u8_order++)
    {
        MAP_ADCSequenceStepConfigure(ADC0_BASE,                          //ADC Base
                                         u8_sequencerNo,                    //Sequence number
                                         u8_order,                              //Sequence order
                                         s_sequence[u8_order].u32_sequence);    //ADC channel
    }
}

/**
  * Function: ADC_peripheral_initializer()
  *
  *  @brief     Enables the Peripherals and configures the Pins for the ADC
  *  @param     No Parameters needed
  *  @retval    No return value
  */
static void ADC_peripheral_initializer(void)
{
    //Enable peripheral associated with AIN HV0-HV7
    MAP_SysCtlPeripheralEnable(AIHV_PERIPH0);

    //Enable peripheral associated with AIN HV8-HV11
    MAP_SysCtlPeripheralEnable(AIHV_PERIPH1);

    //Enable peripheral associated with AIN LV0-LV3
    MAP_SysCtlPeripheralEnable(AILV_PERIPH);

    //Configure AIN HV0-HV7 pins as analog inputs
    MAP_GPIOPinTypeADC(AIHV_PORT0, AIHV0_PIN | AIHV1_PIN | AIHV2_PIN |
                       AIHV3_PIN | AIHV4_PIN | AIHV5_PIN | AIHV6_PIN | AIHV7_PIN);

    //Configure AIN HV8-HV7 pins as analog inputs
    MAP_GPIOPinTypeADC(AIHV_PORT1, AIHV8_PIN | AIHV9_PIN | AIHV10_PIN | AIHV11_PIN);

    //Configure AIN LV0-LV3 pins as analog inputs
    MAP_GPIOPinTypeADC(AILV_PORT, AILV0_PIN | AILV1_PIN | AILV2_PIN | AILV3_PIN);
}


/**
  * Function: ADC_initializer()
  *
  * @This is the ADC initializer code. This function will provide all the initialization for the ADC such that
  * the user does not need to hard code using a bunch of functions.
  * @There should be no parameters of the initializer code.
  * @There is no current return value. Maybe a return value could be put in later to make sure
  * everything is working.
  */
void ADC_initializer()
{
    s_ADCsequence s_sequence0[8] =
    {
                                           {AIHV0_ADC},
                                           {AIHV1_ADC},
                                           {AIHV2_ADC},
                                           {AIHV3_ADC},
                                           {AIHV4_ADC},
                                           {AIHV5_ADC},
                                           {AIHV6_ADC},
                                           {AIHV7_ADC|ADC_CTL_END}
    };
    s_ADCsequence s_sequence1[4] =
    {
                                           {AIHV8_ADC},
                                           {AIHV9_ADC},
                                           {AIHV10_ADC},
                                           {AIHV11_ADC|ADC_CTL_END}
    };
    s_ADCsequence s_sequence2[4] =
    {
                                           {AILV0_ADC},
                                           {AILV1_ADC},
                                           {AILV2_ADC},
                                           {AILV3_ADC|ADC_CTL_END|ADC_CTL_IE}
    /**
     * ADC_CTL_END ---> Tells the ADC that this is the last PIN to be read
     * ADC_CTL_IE ----> Tells the ADC to Interrupt after the pin has been read.
     *
     */
    };



    //Enable the ADC0 peripheral
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);

    while(!MAP_SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0))
    {
        /**Wait for ADC0 to get ready to be configured. Safe to have this so
         * the ADC wont get messed up with the wrong settings, data, ect.
         */

    }

    //Set up Clock for ADC
    ADCClockConfigSet(ADC0_BASE,                //ADC base
                      ADC_CLOCK_SRC_PLL |       //Use PLL as ADC clock source
                      ADC_CLOCK_RATE_FULL,      //Send every sample back to application
                      15);                      //Divide PLL (480MHz) by 15 to get an ADC clock rate of 32MHz


    //Averages 64 ADC samples to provide a higher resolution measurement
    MAP_ADCHardwareOversampleConfigure(ADC0_BASE, s_ADCTable.s_SamplingTable.u32_OversamplingRate);




    ADC_peripheral_initializer();

    //Set the sequences for the ADC
    ADC_sequence_step_initializer(0, 2, 8, s_sequence0);    //Set sequence 0 (ADCHV pins 0-7)
    ADC_sequence_step_initializer(1, 1, 4, s_sequence1);    //Set sequence 1 (ADCHV pins 8-11)
    ADC_sequence_step_initializer(2, 0, 4, s_sequence2);    //Set sequence 2 (ADCLV pins 0-3)

    //Enables ADC sequences 0, 1 and 2
    MAP_ADCSequenceEnable(ADC0_BASE, 0);
    MAP_ADCSequenceEnable(ADC0_BASE, 1);
    MAP_ADCSequenceEnable(ADC0_BASE, 2);

    //Initialize the uDMA
    uDMA_ADC_initializer();

    //CONFIGURE INTERRUPT FOR ANALOG INPUT
    //Clear the sequence 2 interrupt flag before enabling
    MAP_ADCIntClearEx(ADC0_BASE, ADC_INT_SS2);
    //Enable sequence 2 interrupt in peripheral
    MAP_ADCIntEnableEx(ADC0_BASE, ADC_INT_SS2);
    //Enable the sequence 2 interrupt in the NVIC
    MAP_IntEnable(INT_ADC0SS2);

    //Initialize the Timer Interrupt for the ADC
    ADC_Timer1_Initializer();

    analog_mosfets_init(s_ADCTable.s_AnalogInputsTable);

    //Start ADC timer
    MAP_TimerEnable(TIMER1_BASE, TIMER_A);


}

//Interrupt handler for analog inputs
void chpADCHandler(void)
{
    //Get interrupt status
    uint32_t u32_ADC0Status = MAP_ADCIntStatusEx(ADC0_BASE, true);

    uint8_t u8_SafetyCheck;
    //Clear interrupt flag
    //MAP_ADCIntClearEx(ADC0_BASE, u32_ADC0Status);

    //To make sure the transfer is complete
    while(MAP_uDMAChannelModeGet(UDMA_PRI_SELECT|UDMA_CHANNEL_ADC0) != UDMA_MODE_STOP){}

    //Sets transfer parameters for UDMA
    MAP_uDMAChannelTransferSet(UDMA_CHANNEL_ADC0 |                  //ADC sequence 0
                               UDMA_PRI_SELECT,                     //Uses primary data structure
                               UDMA_MODE_BASIC,                     //Performs a basic data transfer
                               (void *)(ADC0_BASE + ADC_O_SSFIFO0), //Source of data transfer
                               &u32_AInBuffer[0],                 //Destination of data transfer
                               8);                                  //Number of data items


    //Sets transfer parameters for UDMA
    MAP_uDMAChannelTransferSet(UDMA_CHANNEL_ADC1 |                  //ADC sequence 1
                               UDMA_PRI_SELECT,                     //Uses primary data structure
                               UDMA_MODE_BASIC,                     //Performs a basic data transfer
                               (void *)(ADC0_BASE + ADC_O_SSFIFO1), //Source of data transfer
                               &u32_AInBuffer[8],             //Destination of data transfer (start at index 8)
                               4);                                  //Number of data items


    //Sets transfer parameters for UDMA
    MAP_uDMAChannelTransferSet(UDMA_CHANNEL_ADC2 |                  //ADC sequence 2
                               UDMA_PRI_SELECT,                     //Uses primary data structure
                               UDMA_MODE_BASIC,                     //Performs a basic data transfer
                               (void *)(ADC0_BASE + ADC_O_SSFIFO2), //Source of data transfer
                               &u32_AInBuffer[12],            //Destination of data transfer (start at index 12)
                               4);                                //Number of data items

    for(u8_SafetyCheck = 0; u8_SafetyCheck < TOTAL_ANALOG_INPUTS; u8_SafetyCheck++)
    {
        if(s_ADCTable.s_AnalogInputsTable[u8_SafetyCheck].b_active == true)
        {
            ADC_Safety_Check(u8_SafetyCheck, u32_AInBuffer[u8_SafetyCheck]);
        }
    }


    //Enable UDMA channel
    MAP_uDMAChannelEnable(UDMA_CHANNEL_ADC0);
    //Enable UDMA channel
    MAP_uDMAChannelEnable(UDMA_CHANNEL_ADC1);
    //Enable UDMA channel
    MAP_uDMAChannelEnable(UDMA_CHANNEL_ADC2);



}







