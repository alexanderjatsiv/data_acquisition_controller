/******************************************************************************/
/**
  * @file           das_safety_test.c
  * @brief          Description of this file.
  */
/*  Dependencies: List all dependencies of this module. */

/*  Revision: */
/*  Jul 2, 2020        Rev/Sprint  Initials  Remarks */ 
/******************************************************************************/
/** @addtogroup _das_safety_test
  * @{
  */ 

/** @defgroup _das_safety_test_Source Source
  * @{
  */

/* Includes *******************************************************************/

#include "hdr/das_safety_test.h"

/* Private Macros *************************************************************/
/** @defgroup _das_safety_test_Private_Macros Private Macros
  * @{
  */

/**
  * @}
  */

/* Private Constants **********************************************************/
/** @defgroup _das_safety_test_Private_Constants Private Constants
  * @{
  */

/**
  * @}
  */

/* Private Variables **********************************************************/
/** @defgroup _das_safety_test_Private_Variables Private Variables
  * @{
  */

uint8_t Safety_Test_Timer = 0;

/**
  * @}
  */

/* Private functions **********************************************************/
/** @defgroup _das_safety_test_Private_Functions Private Functions
  * @{
  */

/*  Function:   functionName */
/**
  *  @brief     Function description
  *  @param     Function parameters
  *  @retval    Return value
  */

/**
  * @}
  */

/* Public functions ***********************************************************/
/** @defgroup _das_safety_test_Public_Functions Public Functions
  * @{
  */


/*
 * Function: Safety_Test_Digital_Logical()
 *
 * @brief       To test the safeties for the Logical Digital Values. Should not output anything
 *              on the safety pins until the systick event happens.
 * @param       No parameters
 * @retval      No return value
 */
void Safety_Test_Digital_Logical()
{
    //Initialize Digital Outputs
    Digital_Outputs_Init();

    uint8_t u8_test;
    for(u8_test = 0; u8_test < 6; u8_test++)
    {
        DOut_Logic_Mode_Set(u8_test, 1); //Set all outputs
    }

    for(u8_test = 6; u8_test < 10; u8_test++)
    {
        DOut_Safety_Mode_Set(u8_test, 1); //Set safeties
        DOut_Safety_Value_Set(u8_test, 0); //Set Output
    }

    //Initialize Digital Inputs
    Digital_Inputs_Init();
    uint8_t u8_testCh[4] = {1,1,1,1};
    for(u8_test = 0; u8_test < 6; u8_test++)
    {
        Digital_Mode(u8_test);
        DIn_Safety_Update(u8_test, SAFETY_LOW, 0, 1, u8_testCh);
    }

}

/*
 * Function: Safety_Test_Digital_Frequency()
 *
 * @brief       To test the safeties for the Frequency Digital Values. Should not output anything
 *              on the safety pins until the systick event happens.
 * @param       No parameters
 * @retval      No return value
 */
void Safety_Test_Digital_Frequency()
{
    //Initialize Digital Outputs
    Digital_Outputs_Init();

    uint8_t u8_test;
    for(u8_test = 0; u8_test < 6; u8_test++)
    {
        DOut_Logic_Mode_Set(u8_test, 1); //Set all outputs
    }
    DOut_PWM_Mode_Set(0);
    DOut_Frequency_Set(0, 1000);

    for(u8_test = 6; u8_test < 10; u8_test++)
    {
        DOut_Safety_Mode_Set(u8_test, 0); //Set safeties
        DOut_Safety_Value_Set(u8_test, 1); //Set Output
    }

    //Initialize Digital Inputs
    Digital_Inputs_Init();
    uint8_t u8_testCh[4] = {1,1,1,1};
    for(u8_test = 0; u8_test < 6; u8_test++)
    {
        Digital_Mode(u8_test);
        //DIn_Safety_Update(u8_test, SAFETY_LOW, 0, 1, u8_testCh);
    }
    Frequency_Mode(0);
    DIn_Safety_Update(0, SAFETY_WINDOW, 700, 1300, u8_testCh);
    Timer4_Init();
}

/*
 * Function: Safety_Test_Analog()
 *
 * @brief       To test the safeties for the Frequency Digital Values. Should not output anything
 *              on the safety pins until the systick event happens.
 * @param       No parameters
 * @retval      No return value
 */
void Safety_Test_Analog()
{
    //Initialize Analog Outputs
    uint8_t u8_Test;
    Analog_Output_Initializer();
    for(u8_Test = 0; u8_Test < 4; u8_Test++)
    {
        switch(u8_Test)
        {
        case 0:
            Analog_Output_Set(0, 410); //Output 2v
            break;
        case 1:
            Analog_Output_Set(1, 410); //Output 2v
            break;
        case 2:
            Analog_Output_Set(2, 205); //Output 2v
            break;
        case 3:
            Analog_Output_Set(3, 205); //Output 2v
            break;
        default:
            break;
        }

    }
    Analog_Output_Switch(0, true);
    Analog_Output_Switch(1, true);
    Analog_Output_Switch(2, true);
    Analog_Output_Switch(3, true);


    //Initialize Analog Inputs

    ADC_initializer();

    uint8_t u8_testCh[4] = {1,1,1,1};

    for(u8_Test = 0; u8_Test < 12; u8_Test++)
    {
        if((u8_Test != 2) && (u8_Test != 9))
        {
        ADC_Mosfet_configure(u8_Test, true);
        ADC_Safety_Update(u8_Test, SAFETY_LOW, 1000, 2500, u8_testCh);
        }

    }
}

/*
 * Function: Logical_Test_Handler()
 *
 * @brief       To test the safeties for the Logical Digital Values. Should not output anything
 *              on the safety pins until the systick event happens.
 * @param       No parameters
 * @retval      No return value
 */
void Logical_Test_Handler()
{
    Safety_Test_Timer++;
    if(Safety_Test_Timer == 100)
    {
        DOut_Logic_Mode_Set(1, 0);
        Safety_Test_Timer = 0;
    }
}

/*
 * Function: Frequency_Test_Handler()
 *
 * @brief       To test the safeties for the Logical Digital Values. Should not output anything
 *              on the safety pins until the systick event happens.
 * @param       No parameters
 * @retval      No return value
 */
void Frequency_Test_Handler()
{
    Safety_Test_Timer++;
    if(Safety_Test_Timer == 100)
    {
        DOut_Frequency_Set(0, 500);
        Safety_Test_Timer = 0;
    }
}


/*
 * Function: AnalogInput_Test_Handler()
 *
 * @brief       To test the safeties for the analog inputs
 * @param       No parameters
 * @retval      No return value
 */
void AnalogInput_Test_Handler()
{
    Safety_Test_Timer++;
    if(Safety_Test_Timer == 100)
    {
        Analog_Output_Set(0, 204); //Output 2v
        Safety_Test_Timer = 0;
    }
}
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
