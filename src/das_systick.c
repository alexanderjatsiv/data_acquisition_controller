/******************************************************************************/
/**
  * @file           das_systick.c
  * @brief          Description of this file.
  */
/*  Dependencies: List all dependencies of this module. */

/*  Revision: */
/*  Jun 25, 2020        Rev/Sprint  Initials  Remarks */ 
/******************************************************************************/
/** @addtogroup _das_systick
  * @{
  */ 

/** @defgroup _das_systick_Source Source
  * @{
  */

/* Includes *******************************************************************/

#include "hdr/das_systick.h"


/* Private Macros *************************************************************/
/** @defgroup _das_systick_Private_Macros Private Macros
  * @{
  */

/**
  * @}
  */

/* Private Constants **********************************************************/
/** @defgroup _das_systick_Private_Constants Private Constants
  * @{
  */

const uint8_t u8_EventInputs = 3;

/**
  * @}
  */

/* Private Variables **********************************************************/
/** @defgroup _das_systick_Private_Variables Private Variables
  * @{
  */

s_Event s_EventTable[3] =
{
 {50, 0, EVENT_RESET, EVENT_ENABLED, Digital_Readall},
 {25, 0, EVENT_RESET, EVENT_ENABLED, DOut_Pin_Handler},
 {50, 0, EVENT_RESET, EVENT_ENABLED, AnalogInput_Test_Handler}

};


/**
  * @}
  */

/* Private functions **********************************************************/
/** @defgroup _das_systick_Private_Functions Private Functions
  * @{
  */

/*
 * Function: Event_Increment(void)
 *
 * @brief   The increments the software timer on the event and checks to see
 *          if the event should be triggered
 * @param   No Parameters
 * @retval  No return value
 *
 */
static void Event_Increment(void)
{
    uint8_t u8_Events;

    for(u8_Events = 0; u8_Events < u8_EventInputs; u8_Events++)
    {
        switch((s_EventTable[u8_Events].b_Enabled) && (~(s_EventTable[u8_Events].b_Event)))
        {
        case true:
            s_EventTable[u8_Events].u16_CurTimems++;
            if(s_EventTable[u8_Events].u16_CurTimems >= s_EventTable[u8_Events].u16_EventTimerms)
            {
                s_EventTable[u8_Events].b_Event = EVENT_TRIGGER;
            }
            break;
        default:
            break;
        }
    }
}

/**
  * @}
  */

/* Public functions ***********************************************************/
/** @defgroup _das_systick_Public_Functions Public Functions
  * @{
  */

/*
 * Function: Event_handler(void)
 *
 * @brief   The systick event handler
 * @param   No Parameters
 * @retval  No return value
 *
 */
void Event_Handler(void)
{
    uint8_t u8_Events;

    for(u8_Events = 0; u8_Events < u8_EventInputs; u8_Events++)
    {
        if(s_EventTable[u8_Events].b_Event == EVENT_TRIGGER)
        {
            s_EventTable[u8_Events].function();
            s_EventTable[u8_Events].u16_CurTimems = 0;
            s_EventTable[u8_Events].b_Event = EVENT_RESET;
        }
    }
}

/*
 * Function: sys_tick_handler(void)
 *
 * @brief   The systick ISR handler
 * @param   No Parameters
 * @retval  No return value
 *
 */

void sys_tick_handler(void)
{
    Event_Increment();
}

/*
 * Function: Sys_Tick_Init(void)
 *
 * @brief   The systick initializer
 * @param   No Parameters
 * @retval  No return value
 *
 */

void Sys_Tick_Init(void)
{
    //Configure SysTick period for 1ms
    MAP_SysTickPeriodSet(4000000 /          //Frequency of PIOSC(16MHz) / 4
                         1000);    //Period of SysTick interrupt
    //Enable SysTick
    MAP_SysTickEnable();

    //Set clock source to PIOSC
    NVIC_ST_CTRL_R ^= NVIC_ST_CTRL_CLK_SRC;

    //Enable SysTick interrupt
    MAP_SysTickIntEnable();
}
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
