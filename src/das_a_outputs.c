/******************************************************************************/
/**
  * @file           das_a_outputs.c
  * @brief          Description of this file.
  */
/*  Dependencies: List all dependencies of this module. */

/*  Revision: */
/*  Jun 4, 2020        Rev/Sprint  Initials  Remarks */ 
/******************************************************************************/
/** @addtogroup _das_a_outputs
  * @{
  */ 

/** @defgroup _das_a_outputs_Source Source
  * @{
  */

/* Includes *******************************************************************/
#include "hdr/das_a_outputs.h"
/* Private Macros *************************************************************/
/** @defgroup _das_a_outputs_Private_Macros Private Macros
  * @{
  */

/**
  * @}
  */

/* Private Constants **********************************************************/
/** @defgroup _das_a_outputs_Private_Constants Private Constants
  * @{
  */

/**
  * @}
  */

/* Private Variables **********************************************************/
/** @defgroup _das_a_outputs_Private_Variables Private Variables
  * @{
  */
s_AnalogOutputs s_AnalogTable[4] =
{
     /*A0*/         {DAC0, false, 0},
     /*A1*/         {DAC1, false, 0},
     /*A2HV*/       {DAC2, false, 0},
     /*A3HV*/       {DAC3, false, 0}
};
/**
  * @}
  */

/* Private functions **********************************************************/
/** @defgroup _das_a_outputs_Private_Functions Private Functions
  * @{
  */

/*  Function:   functionName */
/**
  *  @brief     Function description
  *  @param     Function parameters
  *  @retval    Return value
  */


/*  Function:  DAC_Write
 *
 *  @brief     This function will write to the DAC where it
 *             send the analog signal
 *  @param
 *  @retval    Return value
 */
static void DAC_Write(uint32_t u32_Data)
{
    MAP_SSIDataPutNonBlocking(DAC_SSI_BASE, u32_Data);
    while(MAP_SSIBusy(DAC_SSI_BASE));
}
/*
 * Function: SSI_Config_Init()
 *
 *  @brief     Initializes the SSI
 *  @param     No parameters
 *  @retval    No return value
 */

static void SSI_Config_Init()
{
    //Disable the SSI so a new configuration can be put in
    MAP_SSIDisable(DAC_SSI_BASE);

    //Configure the SSI. This sets the SSI to send the bits
    //in TI mode and makes the controller the Master to the DAC.
    //This also sets the Bit rate to 20Mhz and Data width to 16
    //bit.
    MAP_SSIConfigSetExpClk(DAC_SSI_BASE, SYS_CLOCK_RATE,
                           SSI_FRF_TI, SSI_MODE_MASTER,
                           20000000, 16);

    //Enable the SSI
    MAP_SSIEnable(DAC_SSI_BASE);
}

/**
  * @}
  */

/* Public functions ***********************************************************/
/** @defgroup _das_a_outputs_Public_Functions Public Functions
  * @{
  */

/*  Function:   functionName */
/**
  *  @brief     Function description
  *  @param     Function parameters
  *  @retval    Return value
  */

/*
 * Function:    Analog_Output_TEST0()
 *
 * @brief       Tests to see if the Analog Outputs are working
 * @param       No Param
 * @retval      No return value
 */

void Analog_Output_TEST0()
{
    Analog_Output_Initializer();
    uint8_t u8_TEST0;
    for(u8_TEST0 = 0; u8_TEST0 < 4; u8_TEST0++)
    {
        switch(u8_TEST0)
        {
        case 0:
            s_AnalogTable[u8_TEST0].u16_Output = 204;
            break;
        case 1:
            s_AnalogTable[u8_TEST0].u16_Output = 471;
            break;
        case 2:
            s_AnalogTable[u8_TEST0].u16_Output = 716;
            break;
        case 3:
            s_AnalogTable[u8_TEST0].u16_Output = 1000;
            break;
        default:
            break;
        }

    }
    Analog_Output_Switch(0, true);
    Analog_Output_Switch(1, true);
    Analog_Output_Switch(2, true);
    Analog_Output_Switch(3, true);


}
/*
 * Function:    Analog_Output_TEST1()
 *
 * @brief       Tests to see if the Analog Outputs are working
 * @param       No Param
 * @retval      No return value
 */

void Analog_Output_TEST1()
{
    Analog_Output_Initializer();


    Analog_Output_Switch(0, true);
    Analog_Output_Switch(1, true);
    Analog_Output_Switch(2, true);
    Analog_Output_Switch(3, true);

    Analog_Output_Set(0, 204);
    Analog_Output_Set(1, 471);
    Analog_Output_Set(2, 716);
    Analog_Output_Set(3, 1000);


}
/*
 * Function:    Analog_Output_TEST2()
 *
 * @brief       Tests to see if the Analog Outputs are working
 * @param       No Param
 * @retval      No return value
 */

void Analog_Output_TEST2()
{
    Analog_Output_Initializer();


    Analog_Output_Set(0, 1023);
    Analog_Output_Set(1, 512);
    Analog_Output_Set(2, 237);
    Analog_Output_Set(3, 4000);

    Analog_Output_Switch(0, true);
    Analog_Output_Switch(1, true);
    Analog_Output_Switch(2, true);
    Analog_Output_Switch(3, true);


}
/*
 * Function:    Analog_Output_TEST3()
 *
 * @brief       Tests to see if the Analog Outputs are working
 * @param       No Param
 * @retval      No return value
 */

void Analog_Output_TEST3()
{
    Analog_Output_Initializer();


    Analog_Output_Set(0, 1023);
    Analog_Output_Set(1, 512);
    Analog_Output_Set(2, 237);
    Analog_Output_Set(3, 4000);

    Analog_Output_Switch(0, true);
    Analog_Output_Switch(1, true);
    Analog_Output_Switch(2, true);
    Analog_Output_Switch(3, true);

    Analog_Output_Switch(0,false);
    Analog_Output_Set(1, 3000);
}
/*
 * Function:    Analog_Output_Set()
 *
 * @brief       Sets the Analog Output Values
 * @param       u8_Channel -> The channel to set
 *              u16_OutputValue -> The output value to set the DAC channel to
 * @retval      No return value
 */
void Analog_Output_Set(uint8_t u8_Channel, uint16_t u16_OutputValue)
{
    uint32_t u32_DataSend = 0;
    if(u16_OutputValue < 1023)
    {
        s_AnalogTable[u8_Channel].u16_Output = u16_OutputValue;
    }
    else
    {
        s_AnalogTable[u8_Channel].u16_Output = 1023;
    }
    if(s_AnalogTable[u8_Channel].b_State == true)
    {
        DAC_Write(u32_DataSend|
                  s_AnalogTable[u8_Channel].u16_AnalogOutput|
                  WR_REG_UPDATE|
                  (s_AnalogTable[u8_Channel].u16_Output << 2));
    }
}
/*
 * Function:    Analog_Output_Switch()
 *
 * @brief       Initializes the Analog outputs
 * @param       Channel
 * @retval      No return value
 */

void Analog_Output_Switch(uint8_t u8_channel, bool b_Switch)
{
    uint32_t u32_DataSend = 0;
    switch(b_Switch ^ (s_AnalogTable[u8_channel].b_State))
    {
    case true:
        if(b_Switch == true)
        {
            s_AnalogTable[u8_channel].b_State = true;
            DAC_Write(u32_DataSend |
                      s_AnalogTable[u8_channel].u16_AnalogOutput|
                      WR_REG_UPDATE|
                      (s_AnalogTable[u8_channel].u16_Output << 2));
        }
        else
        {
            s_AnalogTable[u8_channel].b_State = false;
            DAC_Write(u32_DataSend |
                      s_AnalogTable[u8_channel].u16_AnalogOutput|
                      WR_REG_UPDATE);
        }
        break;
    default:
        break;
    }
}
/*
 * Function:    Analog_Output_Initializer()
 *
 * @brief       Initializes the Analog outputs
 * @param       No parameters
 * @retval      No return value
 */

void Analog_Output_Initializer()
{
    //Enable the SSI periph
    MAP_SysCtlPeripheralEnable(DAC_SSI_SYS_PERIPH);

    //Wait for the SSI0 to be ready
    while(!MAP_SysCtlPeripheralReady(DAC_SSI_SYS_PERIPH))
    {
    }

    MAP_SysCtlPeripheralEnable(DAC_GPIO_SYSCTL_PERIPH);

    //Wait for the GPIO to be ready
    while(!MAP_SysCtlPeripheralReady(DAC_GPIO_SYSCTL_PERIPH))
    {

    }

    //Configure the pins that we are using
    MAP_GPIOPinConfigure(GPIO_PA2_SSI0CLK);
    MAP_GPIOPinConfigure(GPIO_PA3_SSI0FSS);
    MAP_GPIOPinConfigure(GPIO_PA4_SSI0XDAT0);

    //Configure them with the SSI
    MAP_GPIOPinTypeSSI(DAC_GPIO_PORT_BASE, DAC_SSI_PINS);

    //Set the Pins to only send 4mA of current and
    //sets the pin type to Weak Pull Down
    MAP_GPIOPadConfigSet(DAC_GPIO_PORT_BASE, DAC_SSI_PINS, GPIO_STRENGTH_4MA,
                     GPIO_PIN_TYPE_STD_WPD);

    //Set the SSI Configurations
    SSI_Config_Init();
}


/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
